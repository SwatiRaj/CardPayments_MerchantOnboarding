import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { AuthenticationService } from './views/authentication/authentication.service';
import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class SharedDataService {

    loginStatus = false;
    private loggedIn = new BehaviorSubject<Boolean>(this.loginStatus);
    loggedInStatus = this.loggedIn.asObservable();


    private userInfoObj = new BehaviorSubject<Object>(null);
    userInfo = this.userInfoObj.asObservable();


    constructor(private authenticationService: AuthenticationService, private router: Router) {
        this.verify()
    }

    verify() {
        if (localStorage.getItem("appToken") != null ? true : false) {
            this.authenticationService.isValidToken().then((response) => {
                this.changeUserInfo(response)
                this.changeLoginStatus(true)
            }).catch((error) => {
                if (error.status == 401) {
                    this.changeLoginStatus(false)
                    localStorage.removeItem("appToken")
                    this.router.navigate(['authenticate']);
                }
            })
        } else {
            this.router.navigate(['authenticate']);
            false;
        }
    }
    changeLoginStatus(loggedIn: Boolean) {
        this.loggedIn.next(loggedIn);
    }
    changeUserInfo(userInfoObj: Object) {
        this.userInfoObj.next(userInfoObj);
    }
}
