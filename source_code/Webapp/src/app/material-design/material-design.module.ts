import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule, MatFormField, MatInputModule, MatToolbarModule, MatTabsModule, MatCardModule, MatSnackBarModule, MatProgressSpinnerModule, MatSidenavModule, MatRadioModule, MatIconModule, MatNavList, MatListModule, MatDatepickerModule, MatNativeDateModule, MatProgressBarModule, MatBadgeModule, MatMenuModule, MatTooltipModule, MatCheckbox, MatCheckboxModule, MatChipsModule, MatAutocompleteModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({

  exports: [
    BrowserAnimationsModule,
    MatRadioModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatButtonModule,
    MatFormFieldModule,
    MatDialogModule,
    MatSelectModule,
    MatInputModule,
    MatToolbarModule,
    MatTabsModule,
    MatCardModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatBadgeModule,
    MatMenuModule,
    MatTooltipModule
  ],
})
export class MaterialDesignModule { }
