import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Http } from '@angular/http';
import { RequestOptions, Headers, Response } from '@angular/http';

@Injectable({
  providedIn: 'root'
})

export class BankRequestService {

  private url = environment.API_URL;

  constructor(private http: Http) { }
  getRequests(): Promise<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Authorization', "Bearer " + localStorage.getItem("appToken"))
    let options = new RequestOptions({ headers: headers });
    return this.http
      .get(this.url + 'bank/requests', options)
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  submitAction(data): Promise<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Authorization', "Bearer " + localStorage.getItem("appToken"))
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post(this.url + 'bank/updatestatus', data, options)
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

}

