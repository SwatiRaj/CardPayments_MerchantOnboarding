import { TestBed, inject } from '@angular/core/testing';

import { BankRequestService } from './bank-request.service';

describe('BankRequestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BankRequestService]
    });
  });

  it('should be created', inject([BankRequestService], (service: BankRequestService) => {
    expect(service).toBeTruthy();
  }));
});
