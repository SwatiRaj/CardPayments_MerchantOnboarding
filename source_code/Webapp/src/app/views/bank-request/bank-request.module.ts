import { ReactiveFormsModule } from '@angular/forms';
import { MaterialDesignModule } from './../../material-design/material-design.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BankRequestComponent } from './bank-request.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialDesignModule,
    ReactiveFormsModule,
    MaterialDesignModule

  ],
  declarations: [BankRequestComponent]
})
export class BankRequestModule { }
