import { TestBed, inject } from '@angular/core/testing';

import { MerchantNewRequestService } from './merchant-new-request.service';

describe('MerchantNewRequestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MerchantNewRequestService]
    });
  });

  it('should be created', inject([MerchantNewRequestService], (service: MerchantNewRequestService) => {
    expect(service).toBeTruthy();
  }));
});
