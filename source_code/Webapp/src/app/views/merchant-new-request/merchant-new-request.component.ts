import { MatSnackBarModule, MatSnackBar } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MerchantNewRequestService } from './merchant-new-request.service';

@Component({
  selector: 'app-merchant-new-request',
  templateUrl: './merchant-new-request.component.html',
  styleUrls: ['./merchant-new-request.component.css']
})
export class MerchantNewRequestComponent implements OnInit {

  chooseBankForm: FormGroup;
  bankList
  spinner = true
  submittedSpinner = false
  documentsList
  bankSelected: Boolean = false
  documentsToSubmit = {}
  selectedBank

  constructor(private service: MerchantNewRequestService, private snackBar: MatSnackBar) {
    this.getBank()
    this.chooseBankForm = new FormGroup({
      bank: new FormControl('')
    });
  }

  ngOnInit() {
  }
  getDocListForBank(bank) {
    this.bankSelected = true
    this.selectedBank = bank
    this.spinner = true
    var data = {
      bankUniqueId: bank[0],
      bankName: bank[1],
      id: bank[2]
    }
    this.service.getDocListForBank(data).then(res => {
      this.documentsList = res.data
      this.spinner = false
      // this.documentsList['NotVerified'].push(this.selectedBank[0])
      console.log("iuui", this.documentsList)
    }).catch(err => {
      this.spinner = false
      this.snackBar.open("Something went wrong. Please try agin later.", '', {
        duration: 2000,
      });
    })
  }

  getBank() {
    this.service.getBanks().then(res => {
      this.bankList = res.data
      this.spinner = false
      console.log(this.bankList)
    }).catch(err => {
      this.spinner = false
      this.snackBar.open("Something went wrong. Please try agin later.", '', {
        duration: 2000,
      });
    })
  }

  close() {
    this.documentsToSubmit = {}
    this.bankSelected = false
    this.selectedBank = null
    this.getBank()
  }


  submit() {
    this.submittedSpinner = true

    var documents = []
    var bankDoc
    if (this.documentsList.NotVerified != null) {
      for (var i = 0; i < this.documentsList.NotVerified.length; i++) {
        if (this.documentsToSubmit[this.documentsList.NotVerified[i]] == null) {
          this.snackBar.open("Please upload all the documents", '', {
            duration: 2000,
          });
          this.submittedSpinner = false
          return
        }
        // if (this.documentsList.NotVerified[i] != this.selectedBank[0])
        documents.push(this.documentsToSubmit[this.documentsList.NotVerified[i]])
        // else {
        // bankDoc = this.documentsToSubmit[this.documentsList.NotVerified[i]]
        // }
      }
    }
    var data = {
      amount: this.documentsList.Amount,
      orgName: this.selectedBank[0],
      documents: documents,
      // bankDoc: bankDoc.data
    }
    console.log(data)
    this.service.submit(data).then(res => {
      this.submittedSpinner = false
      this.snackBar.open("Request has been sent", '', {
        duration: 2000,
      });
      this.close()
    }).catch(err => {
      this.submittedSpinner = false
      this.snackBar.open("Something went wrong. Please try agin later.", '', {
        duration: 2000,
      });
      this.close()
    })
  }
  fileUpload(doc, evt) {
    const files = evt.target.files;
    const file = files[0];
    const file_type = file.type;
    if (files && file) {
      const reader = new FileReader();
      reader.onload = (function (documentsToSubmit, doc) { // here we save variable 'file' in closure
        return function (readerEvt) { // return handler function for 'onload' event
          const binaryString = readerEvt.target.result;
          // console.log(btoa(binaryString));
          documentsToSubmit[doc] = {
            type: doc,
            data: 'data:application/pdf;base64,' + btoa(binaryString)
          }
          console.log(documentsToSubmit)
        }
      })(this.documentsToSubmit, doc);
      reader.readAsBinaryString(file);
    }
  }
}

