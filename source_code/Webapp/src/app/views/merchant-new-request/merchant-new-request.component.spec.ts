import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MerchantNewRequestComponent } from './merchant-new-request.component';

describe('MerchantNewRequestComponent', () => {
  let component: MerchantNewRequestComponent;
  let fixture: ComponentFixture<MerchantNewRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MerchantNewRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MerchantNewRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
