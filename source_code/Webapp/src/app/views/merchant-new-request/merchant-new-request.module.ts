import { MaterialDesignModule } from './../../material-design/material-design.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MerchantNewRequestComponent } from './merchant-new-request.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MaterialDesignModule,
    ReactiveFormsModule,
    FormsModule

  ],
  declarations: [MerchantNewRequestComponent]
})
export class MerchantNewRequestModule { }
