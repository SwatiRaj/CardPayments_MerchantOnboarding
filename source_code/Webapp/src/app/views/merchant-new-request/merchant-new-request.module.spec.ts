import { MerchantNewRequestModule } from './merchant-new-request.module';

describe('MerchantNewRequestModule', () => {
  let merchantNewRequestModule: MerchantNewRequestModule;

  beforeEach(() => {
    merchantNewRequestModule = new MerchantNewRequestModule();
  });

  it('should create an instance', () => {
    expect(merchantNewRequestModule).toBeTruthy();
  });
});
