import { MatSnackBar } from '@angular/material';
import { BankSettingsService } from './bank-settings.service';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bank-settings',
  templateUrl: './bank-settings.component.html',
  styleUrls: ['./bank-settings.component.css']
})
export class BankSettingsComponent implements OnInit {

  cotegorySettingForm: FormGroup;
  documentList = ["Document1", "Document2", "Document3"]

  spinner: Boolean = false;
  constructor(private service: BankSettingsService, private snackBar: MatSnackBar) {
    this.cotegorySettingForm = new FormGroup({
      category1: new FormControl('', Validators.required),
      category1Amount: new FormControl('', Validators.required),
      category2: new FormControl('', Validators.required),
      category2Amount: new FormControl('', Validators.required)
    });
  }



  ngOnInit() {
  }

  submit() {
    this.spinner = true;
    var data = {
      category: [
        {
          name: "MCC1",
          docList: this.cotegorySettingForm.value.category1,
          amount: this.cotegorySettingForm.value.category1Amount
        },
        {
          name: "MCC2",
          docList: this.cotegorySettingForm.value.category2,
          amount: this.cotegorySettingForm.value.category2Amount
        }
      ]
    }


    this.service.submit(data).then(res => {
      // this.cotegorySettingForm = new FormGroup({
      //   category1: new FormControl('', Validators.required),
      //   category1Amount: new FormControl('', Validators.required),
      //   category2: new FormControl('', Validators.required),
      //   category2Amount: new FormControl('', Validators.required)
      // });
      this.spinner = false;
      this.snackBar.open("Request Submitted.", '', {
        duration: 2000,
      });
    }).catch(err => {
      this.spinner = false;
      this.snackBar.open("Something went wrong. Please try agin later.", '', {
        duration: 2000,
      });
    })
  }

}
