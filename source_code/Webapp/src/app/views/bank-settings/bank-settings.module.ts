import { MaterialDesignModule } from './../../material-design/material-design.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BankSettingsComponent } from './bank-settings.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialDesignModule,
    ReactiveFormsModule,
    
  ],
  declarations: [BankSettingsComponent]
})
export class BankSettingsModule { }
