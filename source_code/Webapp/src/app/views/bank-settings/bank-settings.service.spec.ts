import { TestBed, inject } from '@angular/core/testing';

import { BankSettingsService } from './bank-settings.service';

describe('BankSettingsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BankSettingsService]
    });
  });

  it('should be created', inject([BankSettingsService], (service: BankSettingsService) => {
    expect(service).toBeTruthy();
  }));
});
