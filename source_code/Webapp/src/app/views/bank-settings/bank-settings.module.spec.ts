import { BankSettingsModule } from './bank-settings.module';

describe('BankSettingsModule', () => {
  let bankSettingsModule: BankSettingsModule;

  beforeEach(() => {
    bankSettingsModule = new BankSettingsModule();
  });

  it('should create an instance', () => {
    expect(bankSettingsModule).toBeTruthy();
  });
});
