import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdfViewerTestingComponent } from './pdf-viewer-testing.component';

describe('PdfViewerTestingComponent', () => {
  let component: PdfViewerTestingComponent;
  let fixture: ComponentFixture<PdfViewerTestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdfViewerTestingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdfViewerTestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
