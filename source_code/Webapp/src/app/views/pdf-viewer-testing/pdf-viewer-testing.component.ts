import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pdf-viewer-testing',
  templateUrl: './pdf-viewer-testing.component.html',
  styleUrls: ['./pdf-viewer-testing.component.css']
})
export class PdfViewerTestingComponent implements OnInit {

  base64Data

  constructor() { }

  ngOnInit() {
  }

  fileSelectProfilePic(evt) {
    const files = evt.target.files;
    const file = files[0];
    const file_type = file.type;
    if (files && file) {
      const reader = new FileReader();

      reader.onload = this.readerProfilePic.bind(this);

      reader.readAsBinaryString(file);
    }

  }

  readerProfilePic(readerEvt) {
    const binaryString = readerEvt.target.result;
    console.log(btoa(binaryString));
    this.base64Data = 'data:application/pdf;base64,' + btoa(binaryString);
    var win = window.open();
    win.document.write('<iframe src="' + this.base64Data + '" frameborder="0" style="border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;" allowfullscreen></iframe>')

  }

}
