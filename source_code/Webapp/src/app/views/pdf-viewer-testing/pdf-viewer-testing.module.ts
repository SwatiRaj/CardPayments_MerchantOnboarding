import { MaterialDesignModule } from './../../material-design/material-design.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PdfViewerTestingComponent } from './pdf-viewer-testing.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialDesignModule
  ],
  declarations: [PdfViewerTestingComponent]
})
export class PdfViewerTestingModule { }
