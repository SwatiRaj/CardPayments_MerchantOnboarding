import { PdfViewerTestingModule } from './pdf-viewer-testing.module';

describe('PdfViewerTestingModule', () => {
  let pdfViewerTestingModule: PdfViewerTestingModule;

  beforeEach(() => {
    pdfViewerTestingModule = new PdfViewerTestingModule();
  });

  it('should create an instance', () => {
    expect(pdfViewerTestingModule).toBeTruthy();
  });
});
