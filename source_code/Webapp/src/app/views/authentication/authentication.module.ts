import { ReactiveFormsModule } from '@angular/forms';
import { MaterialDesignModule } from './../../material-design/material-design.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationComponent } from './authentication.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialDesignModule,
    ReactiveFormsModule
  ],
  exports: [AuthenticationComponent],
  declarations: [AuthenticationComponent]
})
export class AuthenticationModule { }
