import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Http } from '@angular/http';
import { RequestOptions, Headers, Response } from '@angular/http';

@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {

  private url = environment.API_URL;

  constructor(private http: Http) { }
  login(data): Promise<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post(this.url + 'app/login', data, options)
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  isValidToken() {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Authorization', "Bearer " + localStorage.getItem("appToken"))
    let options = new RequestOptions({ headers: headers });
    return this.http
      .get(this.url + 'app/info', options)
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  signup(data): Promise<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post(this.url + 'app/signup', data, options)
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  // isValidToken() {
  //   let headers = new Headers({ 'Content-Type': 'application/json' });
  //   headers.append('Authorization', "Bearer " + localStorage.getItem("appToken"))
  //   let options = new RequestOptions({ headers: headers });
  //   return this.http
  //     .get(this.url + 'bank/info', options)
  //     .toPromise()
  //     .then(res => {
  //       return res.json();
  //     })
  //     .catch(this.handleError);
  // }

  getCategoryList() {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Authorization', "Bearer " + localStorage.getItem("appToken"))
    let options = new RequestOptions({ headers: headers });
    return this.http
      .get(this.url + 'bank/getCategory', options)
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

}
