import { SharedDataService } from './../../sharedData.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './authentication.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';



@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit {

  loginForm: FormGroup;
  singupFormMerchant: FormGroup;
  singupFormBank: FormGroup;
  singupFormNoca: FormGroup;
  categoryList: Array<String>
  spinner: Boolean = false;
  showLogin: boolean = true;
  maxValidityDate = new Date();

  constructor(private service: AuthenticationService, private sharedDataService: SharedDataService, private snackBar: MatSnackBar, private router: Router) {

    this.getCategoryList()

    this.loginForm = new FormGroup({
      uniqueId: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      role: new FormControl("Merchant", Validators.required)
    });

    this.singupFormMerchant = new FormGroup({
      uniqueId: new FormControl('Merchant1', Validators.required),
      fullName: new FormControl('Merchant One', Validators.required),
      dateOfEstablishment: new FormControl('', Validators.required),
      emailId: new FormControl('merchant1@example.com', Validators.required),
      password: new FormControl('321', Validators.required),
      category: new FormControl('', Validators.required),
      role: new FormControl("Merchant", Validators.required)
    });

    this.singupFormBank = new FormGroup({
      uniqueId: new FormControl('Bank1', Validators.required),
      fullName: new FormControl('Bank One', Validators.required),
      domainName: new FormControl('bank1.com', Validators.required),
      password: new FormControl('321', Validators.required),
      role: new FormControl("Bank", Validators.required)
    });

    this.singupFormNoca = new FormGroup({
      uniqueId: new FormControl('NOCA1', Validators.required),
      fullName: new FormControl('NOCA One', Validators.required),
      domainName: new FormControl('noca1.com', Validators.required),
      password: new FormControl('321', Validators.required),
      role: new FormControl("NOCA", Validators.required)
    });
  }


  ngOnInit() {
  }


  login() {
    var data = this.loginForm.value;
    this.service.login(data).then(res => {
      this.snackBar.open("Login successfull.", '', {
        duration: 2000,
      });


      localStorage.setItem("appToken", res.data.token);
      this.service.isValidToken().then(res => {
        this.sharedDataService.verify()
        if (res.role == "Merchant") {
          this.router.navigate(['/merchant-requests']);
        }
        else if (res.role == "Bank") {
          this.router.navigate(['/bank-requests']);
        } else {
          this.router.navigate(['/noca-requests']);
        }
      }).catch((err) => {
        this.snackBar.open("Something went wrong. Please try agin later.", '', {
          duration: 2000,
        });
      })

    }).catch(err => {
      if (err.status == 401) {
        this.snackBar.open("Invalid credentials. Please try again.", '', {
          duration: 2000,
        });
      }
      else {
        this.snackBar.open("Something went wrong. Please try agin later.", '', {
          duration: 2000,
        });
      }
    })
  }


  signup(role) {
    this.spinner = true;
    console.log(role)
    var formData
    if (role == "Merchant") {
      formData = this.singupFormMerchant.value
    } else if (role == "Bank") {
      formData = this.singupFormBank.value
    }
    else {
      formData = this.singupFormNoca.value
    }
    this.service.signup(formData).then(res => {
      this.spinner = false;
      this.showLogin = true;
      this.snackBar.open("Signup successfull. Please Login.", '', {
        duration: 2000,
      });
    }).catch(err => {
      this.spinner = false;
      if (err.status == 409) {
        this.snackBar.open("Already Registered. Please try to login.", '', {
          duration: 2000,
        });
      }
      else {
        this.snackBar.open("Something went wrong. Please try agin later.", '', {
          duration: 2000,
        });
      }
    })
  }

  getCategoryList() {
    this.service.getCategoryList().then(res => {
      this.categoryList = res.data.category
    }).catch(err => {
      this.snackBar.open("Something went wrong. Please try agin later.", '', {
        duration: 2000,
      });
    })
  }
}
