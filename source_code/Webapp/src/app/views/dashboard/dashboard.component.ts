import { MerchantRequestComponent } from './../merchant-request/merchant-request.component';
import { SharedDataService } from './../../sharedData.service';
import { DashboardService } from './dashboard.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})


export class DashboardComponent implements OnInit {

  userInfo
  mode = new FormControl('side');
  selectedNav
  noOfPendingRequestWithTheOrg: Number;
  dashboardTittle = " Dashboard tittle";
  organizationData
  constructor(private dialog: MatDialog, private router: Router, private activatedRoute: ActivatedRoute,
    private sharedDataService: SharedDataService, private service: DashboardService) {

    this.sharedDataService.userInfo.subscribe(status => {
      this.userInfo = status;
      console.log(this.router.url)
      if (this.router.url == '/') {
        if (this.userInfo.role == "Merchant") {
          this.merchantRequests()
        }
        else if (this.userInfo.role == "Bank") {
          this.bankRequests()
        } else {
          this.nocaRequests()
        }
      }
    });


    if (this.router.url == "/merchant-requests") {
      this.merchantRequests()
    } else if (this.router.url == "/newrequest") {
      this.newRequests()
    }
    else if (this.router.url == "/bank-requests") {
      this.bankRequests()
    }
    else if (this.router.url == "/settings") {
      this.settings()
    }
    else if (this.router.url == "/noca-requests") {
      this.nocaRequests()
    }
  }

  ngOnInit() {

  }
  changeSelectedNav(newNav) {
    this.selectedNav = newNav
  }
  merchantRequests(): void {
    this.dashboardTittle = "Requests"
    this.changeSelectedNav("merchantRequests")
    this.router.navigate(['merchant-requests']);
  }

  bankRequests(): void {
    this.dashboardTittle = "Requests"
    this.changeSelectedNav("bankRequests")
    this.router.navigate(['bank-requests']);
  }

  nocaRequests(): void {
    this.dashboardTittle = "Requests"
    this.changeSelectedNav("nocaRequests")
    this.router.navigate(['noca-requests']);
  }


  newRequests(): void {
    this.dashboardTittle = "New Requests"
    this.changeSelectedNav("newRequests")
    this.router.navigate(['newrequest']);
  }

  settings(): void {
    this.dashboardTittle = "Settings"
    this.changeSelectedNav("settings")
    this.router.navigate(['settings']);
  }


}



