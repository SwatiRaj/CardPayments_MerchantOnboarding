import { BankRequestModule } from './../bank-request/bank-request.module';
import { BankRequestComponent } from './../bank-request/bank-request.component';
import { NocaRequestModule } from './../noca-request/noca-request.module';
import { MerchantRequestModule } from './../merchant-request/merchant-request.module';
import { MerchantNewRequestModule } from './../merchant-new-request/merchant-new-request.module';
import { BankSettingsModule } from './../bank-settings/bank-settings.module';
import { MaterialDesignModule } from './../../material-design/material-design.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';


@NgModule({
  imports: [
    CommonModule,
    MaterialDesignModule,
    RouterModule,
    BankSettingsModule,
    MerchantNewRequestModule,
    MerchantRequestModule,
    BankRequestModule,
    NocaRequestModule

  ],
  entryComponents: [],
  declarations: [DashboardComponent]
})
export class DashboardModule { }
