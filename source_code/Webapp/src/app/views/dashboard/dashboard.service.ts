import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Http } from '@angular/http';
import { RequestOptions, Headers, Response } from '@angular/http';

@Injectable({
  providedIn: 'root'
})

export class DashboardService {

  private url = environment.API_URL;


  constructor(private http: Http) { }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }


}
