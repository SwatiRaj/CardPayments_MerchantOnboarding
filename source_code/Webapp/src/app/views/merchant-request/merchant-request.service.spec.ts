import { TestBed, inject } from '@angular/core/testing';

import { MerchantRequestService } from './merchant-request.service';

describe('MerchantRequestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MerchantRequestService]
    });
  });

  it('should be created', inject([MerchantRequestService], (service: MerchantRequestService) => {
    expect(service).toBeTruthy();
  }));
});
