import { MaterialDesignModule } from './../../material-design/material-design.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MerchantRequestComponent } from './merchant-request.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialDesignModule
  ],
  declarations: [MerchantRequestComponent]
})
export class MerchantRequestModule { }
