import { MerchantRequestService } from './merchant-request.service';
import { MerchantNewRequestService } from './../merchant-new-request/merchant-new-request.service';
import { MatSnackBar } from '@angular/material';
import { BankRequestComponent } from './../bank-request/bank-request.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-merchant-request',
  templateUrl: './merchant-request.component.html',
  styleUrls: ['./merchant-request.component.css']
})
export class MerchantRequestComponent implements OnInit {
  spinner: Boolean = true
  requestsList

  constructor(private service: MerchantRequestService, private snackBar: MatSnackBar) {
    this.getRequests()
  }

  ngOnInit() {
  }

  dateFormat(fulldate) {
    let date = new Date(fulldate);
    return date.getDate() + " - " + date.getMonth() + " - " + date.getFullYear()
  }

  getRequests() {
    this.service.getRequests().then(res => {
      this.requestsList = res.data.map(request => {
        var pay = true
        if (request.PaymentStatus == "APPROVED" || request.Status == "REJECTED") {
          pay = false
        } else {
          for (var i = 0; i < request.docs.length; i++) {
            if (request.docs[i].Status != 'APPROVED' )
              pay = false
          }
        }

        request.pay = pay
        return request
      })
      this.spinner = false
      console.log(this.requestsList)
    }).catch(err => {
      this.spinner = false
      this.snackBar.open("Something went wrong. Please try agin later.", '', {
        duration: 2000,
      });
    })
  }

  viewDocument(data) {
    var base64Data = 'data:application/pdf;base64,' + data;
    var win = window.open();
    win.document.write('<iframe src="' + base64Data + '" frameborder="0" style="border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;" allowfullscreen></iframe>')
  }


  pay(bankUniqueId, status, paymentStatus) {
    var data = {
      bankUniqueId: bankUniqueId,
      status: status,
      paymentStatus: paymentStatus

    }

    console.log(data)
    this.service.pay(data).then(res => {
      this.snackBar.open("Payment is Approved .", '', {
        duration: 2000,
      });
      this.getRequests()
    }).catch(err => {
      this.spinner = false
      this.snackBar.open("Something went wrong. Please try agin later.", '', {
        duration: 2000,
      });
    })
  }

}
