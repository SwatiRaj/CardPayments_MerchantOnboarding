import { MerchantRequestModule } from './merchant-request.module';

describe('MerchantRequestModule', () => {
  let merchantRequestModule: MerchantRequestModule;

  beforeEach(() => {
    merchantRequestModule = new MerchantRequestModule();
  });

  it('should create an instance', () => {
    expect(merchantRequestModule).toBeTruthy();
  });
});
