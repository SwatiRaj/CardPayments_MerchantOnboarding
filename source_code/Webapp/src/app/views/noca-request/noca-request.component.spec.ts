import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NocaRequestComponent } from './noca-request.component';

describe('NocaRequestComponent', () => {
  let component: NocaRequestComponent;
  let fixture: ComponentFixture<NocaRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NocaRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NocaRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
