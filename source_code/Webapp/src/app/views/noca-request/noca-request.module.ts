import { MaterialDesignModule } from './../../material-design/material-design.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NocaRequestComponent } from './noca-request.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialDesignModule,
  ],
  declarations: [NocaRequestComponent]
})
export class NocaRequestModule { }
