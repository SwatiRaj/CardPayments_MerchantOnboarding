import { TestBed, inject } from '@angular/core/testing';

import { NocaRequestService } from './noca-request.service';

describe('NocaRequestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NocaRequestService]
    });
  });

  it('should be created', inject([NocaRequestService], (service: NocaRequestService) => {
    expect(service).toBeTruthy();
  }));
});
