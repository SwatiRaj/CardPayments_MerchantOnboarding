import { MatSnackBar } from '@angular/material';
import { NocaRequestService } from './noca-request.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-noca-request',
  templateUrl: './noca-request.component.html',
  styleUrls: ['./noca-request.component.css']
})
export class NocaRequestComponent implements OnInit {

  spinner: boolean = true
  requestsList
  constructor(private service: NocaRequestService, private snackBar: MatSnackBar) {

    this.getRequests()
  }

  ngOnInit() {
  }



  getRequests() {
    this.service.getRequests().then(res => {
      this.requestsList = res.data
      this.spinner = false
      console.log(this.requestsList)
    }).catch(err => {
      this.spinner = false
      this.snackBar.open("Something went wrong. Please try agin later.", '', {
        duration: 2000,
      });
    })
  }

  viewDocument(data) {
    var base64Data =data;
    var win = window.open();
    win.document.write('<iframe src="' + base64Data + '" frameborder="0" style="border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;" allowfullscreen></iframe>')
  }


  action(merchantId, status) {
    var data = {
      merchantId: merchantId,
      status: status
    }
    this.service.submitAction(data).then(res => {
      this.snackBar.open("Request status changed .", '', {
        duration: 2000,
      });
      this.getRequests()
    }).catch(err => {
      this.spinner = false
      this.snackBar.open("Something went wrong. Please try agin later.", '', {
        duration: 2000,
      });
    })
  }


}
