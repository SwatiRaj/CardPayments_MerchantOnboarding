import { AuthenticationService } from './../../views/authentication/authentication.service';
import { SharedDataService } from './../../sharedData.service';
import { HeaderService } from './header.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  loggedIn: Boolean

  userInfo

  constructor(private sharedDataService: SharedDataService,
    private router: Router,
    private service: HeaderService,
    private authenticationService: AuthenticationService) {
    this.sharedDataService.loggedInStatus.subscribe(status => {
      this.loggedIn = status;
    });
    this.sharedDataService.userInfo.subscribe(status => {
      this.userInfo = status;
    });
  }

  ngOnInit() {
  }

  logout(): void {
    localStorage.removeItem('appToken');
    this.sharedDataService.changeLoginStatus(false)
    this.router.navigate(['/authenticate']);
  }

  organization
}
