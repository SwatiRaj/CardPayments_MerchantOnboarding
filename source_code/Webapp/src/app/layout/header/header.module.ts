import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header.component';
import { MaterialDesignModule } from '../../material-design/material-design.module';
import { SharedDataService } from '../../sharedData.service';

@NgModule({
  imports: [
    CommonModule,
    MaterialDesignModule
  ],
  providers: [SharedDataService],
  exports: [HeaderComponent],
  declarations: [HeaderComponent]
})
export class HeaderModule { }
