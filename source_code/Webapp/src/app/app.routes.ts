import { BankRequestComponent } from './views/bank-request/bank-request.component';
import { MerchantRequestComponent } from './views/merchant-request/merchant-request.component';
import { MerchantNewRequestComponent } from './views/merchant-new-request/merchant-new-request.component';
import { BankSettingsComponent } from './views/bank-settings/bank-settings.component';
import { PdfViewerTestingComponent } from './views/pdf-viewer-testing/pdf-viewer-testing.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';

import { AuthenticationComponent } from './views/authentication/authentication.component';
import { Routes } from "@angular/router/src/config";
import { RouteGuard, NocaRouteGuard, MerchantRouteGuard, BankRouteGuard } from "./app.guard";
import { NocaRequestComponent } from './views/noca-request/noca-request.component';

export const ROUTES: Routes = [

    {
        path: 'authenticate',
        component: AuthenticationComponent
    },
    {
        path: 'pdf',
        component: PdfViewerTestingComponent
    },
    {
        path: '',
        component: DashboardComponent,
        canActivate: [RouteGuard],
        children: [

            // For merchant
            {
                path: 'merchant-requests',
                component: MerchantRequestComponent,
                canActivate: [MerchantRouteGuard],

            },
            {
                path: 'newrequest',
                component: MerchantNewRequestComponent,
                canActivate: [MerchantRouteGuard],

            },

            // For bank
            {
                path: 'bank-requests',
                component: BankRequestComponent,
                canActivate: [BankRouteGuard],

            },
            {
                path: 'settings',
                component: BankSettingsComponent,
                canActivate: [BankRouteGuard],

            },

            // For NOCA
            {
                path: 'noca-requests',
                component: NocaRequestComponent,
                canActivate: [NocaRouteGuard],

            },]
    },
    {
        path: '**',
        redirectTo: 'authenticate',
        pathMatch: 'full'
    },
]