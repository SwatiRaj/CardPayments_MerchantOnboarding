import { AuthenticationService } from './views/authentication/authentication.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class RouteGuard {

    constructor(private router: Router, private authenticationService: AuthenticationService) { }

    canActivate() {
        if (localStorage.getItem("appToken") != null ? true : false) {
            return this.authenticationService.isValidToken().then((response) => true
            ).catch((error) => {
                if (error.status == 401) {
                    localStorage.removeItem("appToken")
                    this.router.navigate(['authenticate']);
                    return false
                }
            })
        } else {
            this.router.navigate(['authenticate']);
            return false;
        }

    }
}
@Injectable()
export class MerchantRouteGuard {

    constructor(private router: Router, private authenticationService: AuthenticationService) { }

    canActivate() {
        if (localStorage.getItem("appToken") != null ? true : false) {
            return this.authenticationService.isValidToken().then((response) => {
                if (response.role != "Merchant") {
                    if (response.role == "NOCA") {
                        this.router.navigate(['/noca-requests']);
                    }
                    else if (response.role == "Bank") {
                        this.router.navigate(['/bank-requests']);
                    }

                }
                return true
            }
            ).catch((error) => {
                if (error.status == 401) {
                    localStorage.removeItem("appToken")
                    this.router.navigate(['authenticate']);
                    return false
                }
            })
        } else {
            this.router.navigate(['authenticate']);
            return false;
        }

    }
}
@Injectable()
export class BankRouteGuard {

    constructor(private router: Router, private authenticationService: AuthenticationService) { }

    canActivate() {
        if (localStorage.getItem("appToken") != null ? true : false) {
            return this.authenticationService.isValidToken().then((response) => {
                if (response.role != "Bank") {
                    if (response.role == "Merchant") {
                        this.router.navigate(['/merchant-requests']);
                    }
                    else if (response.role == "NOCA") {
                        this.router.navigate(['/noca-requests']);
                    }
                }
                return true
            }
            ).catch((error) => {
                if (error.status == 401) {
                    localStorage.removeItem("appToken")
                    this.router.navigate(['authenticate']);
                    return false
                }
            })
        } else {
            this.router.navigate(['authenticate']);
            return false;
        }

    }
}
@Injectable()
export class NocaRouteGuard {

    constructor(private router: Router, private authenticationService: AuthenticationService) { }

    canActivate() {
        if (localStorage.getItem("appToken") != null ? true : false) {
            return this.authenticationService.isValidToken().then((response) => {
                if (response.role != "NOCA") {
                    if (response.role == "Merchant") {
                        this.router.navigate(['/merchant-requests']);
                    }
                    else if (response.role == "Bank") {
                        this.router.navigate(['/bank-requests']);
                    }

                }
                return true
            }
            ).catch((error) => {
                if (error.status == 401) {
                    localStorage.removeItem("appToken")
                    this.router.navigate(['authenticate']);
                    return false
                }
            })
        } else {
            this.router.navigate(['authenticate']);
            return false;
        }

    }
}