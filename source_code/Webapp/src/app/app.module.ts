import { SharedDataService } from './sharedData.service';
import { DashboardModule } from './views/dashboard/dashboard.module';
import { RouteGuard, MerchantRouteGuard, BankRouteGuard, NocaRouteGuard } from './app.guard';
import { ROUTES } from './app.routes';
import { RouterModule } from '@angular/router';
import { HeaderModule } from './layout/header/header.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { AuthenticationModule } from './views/authentication/authentication.module';
import { PdfViewerTestingModule } from './views/pdf-viewer-testing/pdf-viewer-testing.module';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpModule,
    BrowserModule,
    HeaderModule,
    PdfViewerTestingModule,
    DashboardModule,
    AuthenticationModule,
    RouterModule.forRoot(ROUTES)

  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    RouteGuard,
    MerchantRouteGuard,
    BankRouteGuard,
    NocaRouteGuard,
    SharedDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
