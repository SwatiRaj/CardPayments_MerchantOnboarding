var authentication = require('../../config/authentication');

function setUp(router, controllers) {
    router.post('/tan-services/apis/checktan', controllers.tanController.checkTan);
}

module.exports.setUp = setUp;
