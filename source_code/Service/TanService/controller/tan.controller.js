const tanService = require('../service/tan.service');
const Response = require('../util/response');
const tanController = {
    checkTan: checkTan
}

function checkTan(req, res, next) {
    let response = new Response();
    tanService.checkTan(req.body)
        .then((data) => {
            response.data = data;
            response.status.statusCode = '200';
            response.status.message = 'Request Submitted successfully!! ';
            res.status(200).json(response);
        })
        .catch((err) => {
            response.err = err;
            response.status.statusCode = '500';
            response.status.message = 'Server Error!! ';
            res.status(500).json(response);
        });
}

module.exports = tanController;