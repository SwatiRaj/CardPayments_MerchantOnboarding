const Promise = require('bluebird');
var log4js = require('log4js');
var logger = log4js.getLogger('Network service');
const request = require('request');

const tanService = {
    checkTan: checkTan
}

function checkTan(tanObject) {
    return new Promise((resolve, reject) => {
        var externalServiceData = {
            type: tanObject.type,
            merchantId: tanObject.merchantId,
            status: "APPROVED"
        }
        setTimeout(function () {
            request.post("http://localhost:51099/merchant-onboarding-services/apis/changereueststatus",
                { form: externalServiceData }, (err, res, body) => {
                    logger.info("Request status changed");
                });
        }, 30000)
        resolve("Request sent")
    });
}
module.exports = tanService;