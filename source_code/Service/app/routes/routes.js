var authentication = require('../../config/authentication');

function setUp(router, controllers) {
    router.post('/merchant-onboarding-services/apis/app/signup', controllers.appController.signup);
    router.post('/merchant-onboarding-services/apis/app/login', controllers.appController.login);

    router.post('/merchant-onboarding-services/apis/bank/setrules', authentication.authenticate(), controllers.bankController.setRules);
    // router.get('/merchant-onboarding-services/apis/bank/getbanks', authentication.authenticate(), controllers.bankController.getBanks);
    router.get('/merchant-onboarding-services/apis/bank/requests', authentication.authenticate(), controllers.bankController.getRequests);
    router.get('/merchant-onboarding-services/apis/bank/getCategory', controllers.bankController.getCategory);
    router.get('/merchant-onboarding-services/apis/bank/getDocList', authentication.authenticate(), controllers.bankController.getDocList);
    router.post('/merchant-onboarding-services/apis/bank/updatestatus', authentication.authenticate(), controllers.bankController.updateStatus);

    router.get('/merchant-onboarding-services/apis/merchant/requests', authentication.authenticate(), controllers.merchantController.getRequests);
    router.post('/merchant-onboarding-services/apis/merchant/submitrequest', authentication.authenticate(), controllers.merchantController.submitRequest);
    router.post('/merchant-onboarding-services/apis/merchant/getbanksledger', authentication.authenticate(), controllers.merchantController.getBankFromLedger);
    router.get('/merchant-onboarding-services/apis/merchant/getbanks', authentication.authenticate(), controllers.merchantController.getBanks);
    router.post('/merchant-onboarding-services/apis/merchant/getdocforrequest', authentication.authenticate(), controllers.merchantController.getDocForRequest);
    router.post('/merchant-onboarding-services/apis/merchant/updatepaystatus', authentication.authenticate(), controllers.merchantController.updatePayStatus);

    router.get('/merchant-onboarding-services/apis/noca/requests', authentication.authenticate(), controllers.nocAuthorityController.getRequests);
    router.post('/merchant-onboarding-services/apis/noca/submitaction', authentication.authenticate(), controllers.nocAuthorityController.submitAction);

    router.post('/merchant-onboarding-services/apis/network/login', controllers.networkController.login);
    router.post('/merchant-onboarding-services/apis/network/invoke', controllers.networkController.invokeChaincode);
    router.post('/merchant-onboarding-services/apis/network/channel', controllers.networkController.createChannel);
    router.post('/merchant-onboarding-services/apis/network/chaincode/upgrade', controllers.networkController.upgradeChaincode);

    router.post('/merchant-onboarding-services/apis/sendtanrequest', controllers.appController.sendTanRequests);
    router.post('/merchant-onboarding-services/apis/changereueststatus', controllers.appController.changeRequestStatus);

    router.get('/merchant-onboarding-services/apis/app/info', authentication.authenticate(), controllers.appController.getInfo);

}

module.exports.setUp = setUp;
