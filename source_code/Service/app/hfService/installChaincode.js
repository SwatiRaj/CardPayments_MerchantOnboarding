'use strict';
var path = require('path');
var util = require('util');
var config = require('../../config/hfcConfig');
var helper = require('./helper');
var logger = helper.getLogger('install-chaincode');
var tx_id = null;

var installChaincode = async function (peers, chaincodeName, chaincodeVersion, chaincodePath, org, userName) {
	logger.debug('\n\n============ Install chaincode on organizations ============\n');
	helper.setupChaincodeDeploy();
	let error_message = null;
	try {

		var chaincodeType = "golang";

		var client = await helper.getClientForOrg(org, userName);
		logger.debug('Successfully got the fabric client for the organization "%s"', org);

		var request = {
			targets: peers,
			chaincodePath: chaincodePath,
			chaincodeId: chaincodeName,
			chaincodeVersion: chaincodeVersion,
			chaincodeType: chaincodeType,
			tx_id: client.newTransactionID(true) //get an admin transactionID
		};
		let results = await client.installChaincode(request);
		// the returned object has both the endorsement results
		// and the actual proposal, the proposal will be needed
		// later when we send a transaction to the orederer
		var proposalResponses = results[0];
		var proposal = results[1];

		// lets have a look at the responses to see if they are
		// all good, if good they will also include signatures
		// required to be committed
		var all_good = true;
		for (var i in proposalResponses) {
			let one_good = false;
			if (proposalResponses && proposalResponses[i].response &&
				proposalResponses[i].response.status === 200) {
				one_good = true;
				logger.info('install proposal was good');
			} else {
				logger.error('install proposal was bad %j', proposalResponses.toJSON());
			}
			all_good = all_good & one_good;
		}
		if (all_good) {
			logger.info('Successfully sent install Proposal and received ProposalResponse');
		} else {
			error_message = 'Failed to send install Proposal or receive valid response. Response null or status is not 200'
			logger.error(error_message);
		}
	} catch (error) {
		logger.error('Failed to install due to error: ' + error.stack ? error.stack : error);
		error_message = error.toString();
	}

	if (!error_message) {
		let message = util.format('Successfully install chaincode');
		logger.info(message);
		// build a response to send back to the REST caller
		let response = {
			success: true,
			message: message
		};
		return { status: "SUCCESS" }
	} else {
		let message = util.format('Failed to install due to:%s', error_message);
		logger.error(message);
		return { status: "ERROR", error: err.toString() }
	}

};

exports.installChaincode = installChaincode;
