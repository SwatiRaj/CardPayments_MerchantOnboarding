var util = require('util');
var fs = require('fs');
var path = require('path');
var helper = require('./helper');
var log4js = require('log4js');
var logger = log4js.getLogger('Consortium service');
var superagent = require('superagent');
var http = require('http');
var networkConfig = require('../../artifacts/networkConfig/networkConfig.json');

var ipAddress = networkConfig.ipAddress;
var configtxlatorPort = networkConfig.configtxlatorPort;


var createConsortium = async function (consortiumName, consortiumOrgs, ordererName, org, userName) {
	var newConsortium = JSON.parse(fs.readFileSync('artifacts/channel/templates/template-consortium.json'));
	logger.debug('\n====== Adding Org to the Consortium ======\n');
	try {
		// Orderer info
		var orderer = networkConfig.orderers[ordererName];
		var private = fs.readFileSync(orderer.keystore).toString();
		var cert = fs.readFileSync(orderer.signcerts).toString();
		var msp = ordererName + "MSP";
		var channelName = "testchainid";
		var client = await helper.getClientForOrg(org, userName);
		logger.debug('Successfully got the fabric client for the organization "%s"', org);

		var channel = client.getChannel(channelName);
		if (!channel) {
			let message = util.format('Channel %s was not defined in the connection profile', channelName);
			logger.error(message);
			throw new Error(message);
		}

		// Signature of the Orderer inorder to get the Configuration Block from System Channel (testchainid)
		client.setAdminSigningIdentity(private, cert, msp);
		//End
		var fetched = false
		var configBlock = await channel.getChannelConfigFromOrderer();

		var config = await superagent.post('http://' + ipAddress + ':' + configtxlatorPort + '/protolator/decode/common.Config',
			configBlock.config.toBuffer())
			.buffer()
			.then(res => {
				return res.text;
			});

		var configJson = JSON.parse(config);
		//  fs.writeFileSync('config.json', JSON.stringify(configJson));
		var modifiedConfigJson = JSON.parse(config);
		// fs.writeFileSync('consortiumTemplate.json', JSON.stringify(modifiedConfigJson.channel_group.groups.Consortiums.groups.defaultConsortium));
		logger.debug("*************  consortium orgs ", consortiumOrgs)
		consortiumOrgs.forEach(org => {
			newConsortium.groups[org + "MSP"] = JSON.parse(fs.readFileSync('artifacts/organizations/' + org + '/' + org + '.config.json', "utf8"));
		});

		// fs.writeFileSync('newConsortium.json', JSON.stringify(newConsortium));
		modifiedConfigJson.channel_group.groups.Consortiums.groups[consortiumName] = newConsortium
		// fs.writeFileSync('config.json', JSON.stringify(configJson));
		// fs.writeFileSync('modified_config.json', JSON.stringify(modifiedConfigJson));

		var configPb = await superagent.post('http://' + ipAddress + ':' + configtxlatorPort + '/protolator/encode/common.Config  ', JSON.stringify(configJson))
			.buffer()
			.then(res => {
				// fs.writeFileSync('config.pb', res.body);
				return res.body;
			}).catch(err => {
				throw err;
			});

		var modifiedConfigPb = await superagent.post('http://' + ipAddress + ':' + configtxlatorPort + '/protolator/encode/common.Config  ', JSON.stringify(modifiedConfigJson))
			.buffer()
			.then(res => {
				// fs.writeFileSync('modifiedConfig.pb', res.body);
				return res.body;
			}).catch(err => {
				throw err;
			});

		var updatedConfigPb = await superagent.post('http://' + ipAddress + ':' + configtxlatorPort + '/configtxlator/compute/update-from-configs')
			.buffer()
			.attach('original', configPb, 'originalFile')
			.attach('updated', modifiedConfigPb, 'updatedFile')
			.field('channel', channelName)
			.then(res => {
				// fs.writeFileSync('updatedConfig.pb', res.body);
				return res.body;
			}).catch(err => {
				throw err;
			});

		var updatedConfigJson = await superagent.post('http://' + ipAddress + ':' + configtxlatorPort + '/protolator/decode/common.ConfigUpdate', updatedConfigPb)
			.buffer()
			.then(res => {
				// fs.writeFileSync('updatedConfig.json', res.text);
				return JSON.parse(res.text);
			}).catch(err => {
				throw err;
			});

		var updatedConfigEnvelopeJson = {
			payload: {
				header: {
					channel_header: {
						channel_id: channelName, type: 2
					}
				}, data: {
					config_update: updatedConfigJson
				}
			}
		}
		//fs.writeFileSync('updatedConfigEnvelope.json', JSON.stringify(updatedConfigEnvelopeJson));

		var updatedConfigEnvelopePb = await superagent.post('http://' + ipAddress + ':' + configtxlatorPort + '/protolator/encode/common.Envelope', JSON.stringify(updatedConfigEnvelopeJson))
			.buffer()
			.then(res => {
				// fs.writeFileSync('updatedConfigEnvelope.pb', res.body);
				return res.body;
			}).catch(err => {
				throw err;
			});

		var channelConfig = client.extractChannelConfig(updatedConfigEnvelopePb);

		var signatures = [];
		let signature = client.signChannelConfig(channelConfig);
		signatures.push(signature);

		let request = {
			config: channelConfig,
			signatures: signatures,
			name: channelName,
			txId: client.newTransactionID(true) // get an admin based transactionID
		};

		// send to orderer
		var response = await client.updateChannel(request)
		logger.debug(' response ::%j', response);
		if (response && response.status === 'SUCCESS') {
			logger.debug('Successfully created consortium "' + consortiumName + '"');
			let response = {
				success: true,
				message: 'Channel \'' + channelName + '\' updated Successfully'
			};
			return { status: "SUCCESS" }
		} else {
			logger.error('\n!!!!!!!!! Failed to create consortium \'' + consortiumName +
				'\' !!!!!!!!!\n\n');
			throw new Error('Failed to created consortium \'' + consortiumName + '\'');
		}

	} catch (err) {
		logger.error('Failed to update the channel: ' + err.stack ? err.stack : err);
		return { status: "ERROR", error: err.toString() }
	}

};

var checkConsortium = async function (consortiumName, ordererName, org, userName) {
	logger.debug('\n====== Check Consortium created ======\n');
	try {
		// Orderer info
		var orderer = networkConfig.orderers[ordererName];
		var private = fs.readFileSync(orderer.keystore).toString();
		var cert = fs.readFileSync(orderer.signcerts).toString();
		var msp = ordererName + "MSP";
		var channelName = "testchainid";
		var client = await helper.getClientForOrg(org, userName);
		logger.debug('Successfully got the fabric client for the organization "%s"', org);

		var channel = client.getChannel(channelName);
		if (!channel) {
			let message = util.format('Channel %s was not defined in the connection profile', channelName);
			logger.error(message);
			throw new Error(message);
		}

		// Signature of the Orderer inorder to get the Configuration Block from System Channel (testchainid)
		client.setAdminSigningIdentity(private, cert, msp);
		//End

		let configBlock = await channel.getChannelConfigFromOrderer();

		var config = await superagent.post('http://' + ipAddress + ':' + configtxlatorPort + '/protolator/decode/common.Config',
			configBlock.config.toBuffer())
			.buffer()
			.then(res => {
				return res.text;
			});
		var configJson = JSON.parse(config);
		logger.debug("********************** Check consortium " + consortiumName + "***************************")
		if (configJson != null && configJson.channel_group != null && configJson.channel_group.groups != null && configJson.channel_group.groups.Consortiums.groups != null && configJson.channel_group.groups.Consortiums.groups[consortiumName] != null) {
			logger.debug("********************** consortium found ****************************")
			return { status: "FOUND" };
		} else {
			logger.debug("********************** consortium not found ****************************")
			return { status: "NOT_FOUND" };
		}
	} catch (err) {
		logger.error('Failed get the system channel: ' + err.stack ? err.stack : err);
		return { status: "ERROR", error: err.toString() }
	}
}

exports.createConsortium = createConsortium;
exports.checkConsortium = checkConsortium;

