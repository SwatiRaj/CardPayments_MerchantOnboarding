'use strict';
var log4js = require('log4js');
var logger = log4js.getLogger('Helper');
logger.setLevel('DEBUG');
var path = require('path');
var util = require('util');
var hfc = require('fabric-client');
hfc.setLogger(logger);

var sleep = async function (sleep_time_ms) {
	return new Promise(resolve => setTimeout(resolve, sleep_time_ms));
}

async function getClientForOrg(userorg, username) {
	logger.debug('getClientForOrg - ****** START %s %s', userorg, username)
	let config = '-connection-profile-path';
	let client = hfc.loadFromConfig(hfc.getConfigSetting('network' + config));
	client.loadFromConfig(hfc.getConfigSetting(userorg + config));
	await client.initCredentialStores();

	if (username) {
		await client.getUserContext(username, true);
	}
	logger.debug('getClientForOrg - ****** END %s %s \n\n', userorg, username)

	return client;
}

var setupChaincodeDeploy = function () {
	process.env.GOPATH = path.join(__dirname, hfc.getConfigSetting('CC_SRC_PATH'));
};

var getLogger = function (moduleName) {
	var logger = log4js.getLogger(moduleName);
	logger.setLevel('DEBUG');
	return logger;
};

var helperService = {
	getClientForOrg: getClientForOrg,
	getLogger: getLogger,
	setupChaincodeDeploy: setupChaincodeDeploy,
}

module.exports = helperService;