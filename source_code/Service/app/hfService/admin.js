var helper = require('./helper');
var log4js = require('log4js');
var logger = log4js.getLogger('Admin Service');
logger.setLevel('DEBUG');
var hfc = require('fabric-client');
var hfca = require('fabric-ca-client');
hfc.setLogger(logger);

var getOrRegisterAdmin = async (org) => {

    try {
        var client = await helper.getClientForOrg(org)
        var admins = hfc.getConfigSetting('admins');
        var adminObj = await client.getUserContext(admins[0].username, true);
        if (adminObj && adminObj.isEnrolled()) {
            logger.info('Successfully loaded Admin of ' + org + ' from persistence');
        } else {
            let caClient = client.getCertificateAuthority();
            let enrollment = await caClient.enroll({
                enrollmentID: admins[0].username,
                enrollmentSecret: admins[0].secret
            })

            var adminObj = await client.createUser(
                {
                    username: admins[0].username,
                    mspid: org + 'MSP',
                    cryptoContent: { privateKeyPEM: enrollment.key.toBytes(), signedCertPEM: enrollment.certificate }
                });
            // Creating new affiliation
            // await caClient.newAffiliationService().create({ "name": org.toLowerCase() }, adminObj)
            // await caClient.newAffiliationService().create({ "name": org.toLowerCase() + ".swap" }, adminObj)
            await caClient.newAffiliationService().create({ "name": org.toLowerCase() + '-merchantonboarding' }, adminObj)
            adminObj.setRoles(['Admin'])
            adminObj = await client.setUserContext(adminObj);
            logger.info('Successfully registered Admin of ' + org + ' and loaded ');
        }
        logger.debug("Admin Object : " + adminObj);
        return { status: "SUCCESS", user: adminObj }
    } catch (err) {
        logger.error('Failed to to get the Admin : ' + err.stack ? err.stack : err);
        return { status: "ERROR", error: err.toString() }
    }

}
module.exports.getAdmin = getOrRegisterAdmin;
