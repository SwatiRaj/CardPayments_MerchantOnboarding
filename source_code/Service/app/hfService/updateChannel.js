var util = require('util');
var fs = require('fs');
var path = require('path');
var helper = require('./helper');
var logger = helper.getLogger('Update-Channel');
var superagent = require('superagent');
var http = require('http');
var hfc = require('fabric-client');


var updateChannel = async function (addOrg) {
	try {

		var networkConfig = JSON.parse(fs.readFileSync('artifacts/networkConfig/networkConfig.json'));
		var ipAddress = networkConfig.ipAddress;
		var configtxlatorPort = networkConfig.configtxlatorPort;

		var channelName = hfc.getConfigSetting('channelName');

		var orgs = ((networkConfig).channels[channelName].organizations).toString().split(":");
		var uname = hfc.getConfigSetting('admins')[0].username
		logger.debug('\n====== Updating Channel \'' + channelName + '\' ======\n');

		var client = await helper.getClientForOrg(orgs[0], uname);
		logger.debug('Successfully got the fabric client for the organization "%s"', orgs[0]);

		var channel = client.getChannel(channelName);
		if (!channel) {
			let message = util.format('Channel %s was not defined in the connection profile', channelName);
			logger.error(message);
			throw new Error(message);
		}

		let configBlock = await channel.getChannelConfigFromOrderer();

		var config = await superagent.post('http://' + ipAddress + ':' + configtxlatorPort + '/protolator/decode/common.Config',
			configBlock.config.toBuffer())
			.buffer()
			.then(res => {
				return res.text;
			});


		var configJson = JSON.parse(config);
		var modifiedConfigJson = JSON.parse(config);
		var orgJson = JSON.parse(fs.readFileSync('artifacts/organizations/' + addOrg + '/' + addOrg + '.config.json', "utf8"));
		if (modifiedConfigJson.channel_group.groups.Application.groups == null)
			modifiedConfigJson.channel_group.groups.Application.groups = {}
		modifiedConfigJson.channel_group.groups.Application.groups[addOrg + "MSP"] = orgJson
		// fs.writeFileSync('config.json', JSON.stringify(configJson));
		// fs.writeFileSync('modified_config.json', JSON.stringify(modifiedConfigJson));


		var configPb = await superagent.post('http://' + ipAddress + ':' + configtxlatorPort + '/protolator/encode/common.Config  ', JSON.stringify(configJson))
			.buffer()
			.then(res => {
				// fs.writeFileSync('config.pb', res.body);
				return res.body;
			}).catch(err => {
				throw err;
			});


		var modifiedConfigPb = await superagent.post('http://' + ipAddress + ':' + configtxlatorPort + '/protolator/encode/common.Config  ', JSON.stringify(modifiedConfigJson))
			.buffer()
			.then(res => {
				// fs.writeFileSync('modifiedConfig.pb', res.body);
				return res.body;
			}).catch(err => {
				throw err;
			});

		var updatedConfigPb = await superagent.post('http://' + ipAddress + ':' + configtxlatorPort + '/configtxlator/compute/update-from-configs')
			.buffer()
			.attach('original', configPb, 'originalFile')
			.attach('updated', modifiedConfigPb, 'updatedFile')
			.field('channel', channelName)
			.then(res => {
				// fs.writeFileSync('updatedConfig.pb', res.body);
				return res.body;
			}).catch(err => {
				throw err;
			});


		var updatedConfigJson = await superagent.post('http://' + ipAddress + ':' + configtxlatorPort + '/protolator/decode/common.ConfigUpdate', updatedConfigPb)
			.buffer()
			.then(res => {
				// fs.writeFileSync('updatedConfig.json', res.text);
				return JSON.parse(res.text);
			}).catch(err => {
				throw err;
			});

		var updatedConfigEnvelopeJson = {
			payload: {
				header: {
					channel_header: {
						channel_id: channelName, type: 2
					}
				}, data: {
					config_update: updatedConfigJson
				}
			}
		}
		//fs.writeFileSync('updatedConfigEnvelope.json', JSON.stringify(updatedConfigEnvelopeJson));


		var updatedConfigEnvelopePb = await superagent.post('http://' + ipAddress + ':' + configtxlatorPort + '/protolator/encode/common.Envelope', JSON.stringify(updatedConfigEnvelopeJson))
			.buffer()
			.then(res => {
				// fs.writeFileSync('updatedConfigEnvelope.pb', res.body);
				return res.body;
			}).catch(err => {
				throw err;
			});

		var channelConfig = client.extractChannelConfig(updatedConfigEnvelopePb);

		var signatures = [];
		var orgs = ((networkConfig).channels[channelName].organizations).toString().split(":");
		logger.info("Organizations ================= > ")
		logger.info(orgs)

		for (var i = 0; i < orgs.length; i++) {
			var clientForSign = await helper.getClientForOrg(orgs[i], uname);
			let signature = clientForSign.signChannelConfig(channelConfig);
			signatures.push(signature);
		}
		logger.info("Signatures ================= > ")
		logger.info(signatures)

		let request = {
			config: channelConfig,
			signatures: signatures,
			name: channelName,
			txId: client.newTransactionID(true) // get an admin based transactionID
		};

		// send to orderer
		var response = await client.updateChannel(request)
		logger.debug(' response ::%j', response);
		if (response && response.status === 'SUCCESS') {
			logger.debug('Successfully updated the channel.');
			let response = {
				success: true,
				message: 'Channel \'' + channelName + '\' updated Successfully'
			};
			return { status: "SUCCESS" }

		} else {
			logger.error('\n!!!!!!!!! Failed to updated the channel \'' + channelName +
				'\' !!!!!!!!!\n\n');
			throw new Error('Failed to updated the channel \'' + channelName + '\'');
		}
	} catch (err) {
		logger.error('Failed to update the channel: ' + err.stack ? err.stack : err);
		return { status: "ERROR", error: err.toString() }
	}

};




exports.updateChannel = updateChannel;

