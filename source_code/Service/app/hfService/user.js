'use strict';
var log4js = require('log4js');
var logger = log4js.getLogger('Register User');
logger.setLevel('DEBUG');
var Promise = require('bluebird');
var path = require('path');
var util = require('util');
var hfc = require('fabric-client');
hfc.setLogger(logger);
var helper = require('./helper');
var Admin = require('./admin');

var registerAndEnrollUser = async function (username, userOrg) {
    try {
        var message;
        var client = await helper.getClientForOrg(userOrg);
        logger.debug('Successfully initialized the credential stores');
        var user = await client.getUserContext(username, true);
        if (user && user.isEnrolled()) {
            logger.info('User Already exists');
            return { status: "ERROR", code: 409 }
        } else {
            logger.info('Register and Enroll User, so we will need an admin user object to register', username);
            var admins = hfc.getConfigSetting('admins');
            var adminUserObj = await Admin.getAdmin(userOrg);
            let caClient = client.getCertificateAuthority();
            let secret = await caClient.register(({
                enrollmentID: username,
                // affiliation: userOrg.toLowerCase() + '.department1',
                affiliation: userOrg.toLowerCase() + '-merchantonboarding',
                role: 'User'
            }), adminUserObj);

            logger.debug("secret : " + secret);
            let enrollment = await caClient.enroll(({
                enrollmentID: username,
                enrollmentSecret: secret
            }));
            logger.debug('Successfully got the secret for user %s', username);

            var userObj = await client.createUser(
                {
                    username: username,
                    mspid: userOrg + 'MSP',
                    cryptoContent: { privateKeyPEM: enrollment.key.toBytes(), signedCertPEM: enrollment.certificate }
                });
            userObj.setRoles(['User'])
            user = await client.setUserContext(userObj);
            logger.debug('Successfully enrolled username %s  and setUserContext on the client object', username);
            message = " enrolled Successfully"
            logger.debug("User Object : " + user);
        }
        if (user && user.isEnrolled) {
            return { status: "SUCCESS", user: user }
        } else {
            throw new Error('User was not enrolled ');
        }
    } catch (error) {
        logger.error('Failed to register user: %s with error: %s', username, error.toString());
        return { status: "ERROR", error: error.toString() }
    }

};

var getUser = async function (userEmail, org) {
    try {
        var client = await helper.getClientForOrg(org)
        var user = await client.getUserContext(userEmail, true);
        if (user && user.isEnrolled()) {
            logger.info('Successfully loaded user ' + userEmail);
            return { status: "SUCCESS", user: user }
        } else {
            return { status: "ERROR", error: "Not_Found" }
        }
    } catch (error) {
        logger.error('Failed to get registered user: %s with error: %s', username, error.toString());
        return { status: "ERROR", error: error.toString() }
    }
}

module.exports.getUser = getUser;
module.exports.registerAndEnrollUser = registerAndEnrollUser;
