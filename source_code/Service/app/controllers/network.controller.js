var Response = require('../util/response');
var networkService = require('../services/network.service');
var log4js = require('log4js');
var logger = log4js.getLogger('Network Controller');

var networkController = {
    login: login,
    createChannel: createChannel,
    upgradeChaincode: upgradeChaincode,
    invokeChaincode: invokeChaincode,
    upgradePrivateChaincode: upgradePrivateChaincode,
};

function login(req, res) {
    var response = new Response();
    networkService.login(req.body).then(function (token) {
        response.data.token = token;
        response.status.code = "200";
        response.status.message = "Login successful.";
        res.status(response.status.code).json(response);
    }).catch(function (error) {
        if (error.code == 401) {
            response.status.code = "401";
            response.status.message = "Unauthorized";
            res.status(response.status.code).json(response);
        }
        else {
            logger.error(req.route.stack[0].method, req.route.path, error)
            response.status.code = "500";
            response.status.message = "Login failed. Please try again.";
            res.status(response.status.code).json(response);
        }
    });
}


function createChannel(req, res) {
    var response = new Response();
    networkService.createChannel(req.body.orgs).then(function (result) {
        response.data = result;
        response.status.code = "200";
        response.status.message = "Channel creation successful.";
        res.status(response.status.code).json(response);
    }).catch(function (error) {
        logger.error(req.route.stack[0].method, req.route.path, error)
        response.status.code = "500";
        response.status.message = "Create channel failed. Please try again.";
        res.status(response.status.code).json(response);
    });
}


function invokeChaincode(req, res) {
    var response = new Response();
    networkService.invokeChaincode(req.body.org).then(function (result) {
        response.data = result;
        response.status.code = "200";
        response.status.message = "Invoke successful.";
        res.status(response.status.code).json(response);
    }).catch(function (error) {
        logger.error(req.route.stack[0].method, req.route.path, error)
        response.status.code = "500";
        response.status.message = "Invoke failed. Please try again.";
        res.status(response.status.code).json(response);
    });
}

// Common Chaincode
function upgradeChaincode(req, res) {

    var response = new Response();
    logger.debug('==================== UPGRADE CHAINCODE ON A CHANNEL ==================');

    networkService.upgradeChaincode().then(function (result) {
        response.data = result;
        response.status.code = "200";
        response.status.message = " Chaincode Upgrade Successful.";
        res.status(response.status.code).json(response);
    }).catch(function (error) {
        logger.error(req.route.stack[0].method, req.route.path, error)
        response.status.code = "500";
        response.status.message = "Chaincode Upgrade failed. Please try again.";
        res.status(response.status.code).json(response);

    });
}

// Private chaincode
function upgradePrivateChaincode(req, res) {

    var response = new Response();
    logger.debug('==================== UPGRADE CHAINCODE ON A CHANNEL ==================');

    networkService.upgradePrivateChaincode().then(function (result) {
        response.data = result;
        response.status.code = "200";
        response.status.message = " Chaincode Upgrade Successful.";
        res.status(response.status.code).json(response);
    }).catch(function (error) {
        logger.error(req.route.stack[0].method, req.route.path, error)
        response.status.code = "500";
        response.status.message = "Chaincode Upgrade failed. Please try again.";
        res.status(response.status.code).json(response);

    });
}
module.exports = networkController;