var appService = require("../services/app.service");
var Response = require("../util/response");
var log4js = require('log4js');
var logger = log4js.getLogger('App Controller');
var Promise = require("bluebird");

var appControler = {
    signup: signup,
    login: login,
    changeRequestStatus: changeRequestStatus,
    sendTanRequests: sendTanRequests,
    getInfo: getInfo
};

/**
 * Rgister an organization or merchant
 */
function signup(req, res) {
    var response = new Response();
    var registerObject = req.body;
    appService.signup(registerObject).then(function (registerOrgResponse) {
        response.status.statusCode = '200';
        response.status.message = 'Merchant or organization registered successfully!! ';
        res.status(200).json(response);
    }).catch(function (err) {
        logger.error(err)
        if (err.code == 409) {
            response.status.code = "409";
            response.status.message = "Merchant  or organization already registered";
            res.status(response.status.code).json(response);
        } else if (err.code == 404) {
            response.status.code = "409";
            response.status.message = err.message;
            res.status(response.status.code).json(response);
        } else {
            response.status.code = "500";
            response.status.message = "Signup failed. Please try again.";
            res.status(response.status.code).json(response);
        }
    })
}

function getInfo(req, res) {
    var response = new Response();
    response.status.statusCode = '200';
    response.status.message = 'Merchant or organization info fetched successfully!! ';
    res.status(200).json(req.user);
}
/**
 * Login Organization or merchant
 */
function login(req, res) {
    var response = new Response();
    var loginObject = req.body;
    appService.login(loginObject).then(function (token) {
        response.data.token = token;
        response.status.statusCode = '200';
        response.status.message = 'Organization or Merchant logged in successfully!! ';
        res.status(200).json(response);
    }).catch(function (err) {
        logger.info(err)
        if (err.code == 401) {
            response.status.code = "401";
            response.status.message = "Unauthorized";
            res.status(response.status.code).json(response);
        }
        else {
            response.status.code = "500";
            response.status.message = "Login failed. Please try again.";
            res.status(response.status.code).json(response);
        }
    })
}

function changeRequestStatus(req, res) {
    var response = new Response();
    appService.changeRequestStatus(req.body).then(function (data) {
        response.data = data;
        response.status.statusCode = '200';
        response.status.message = 'Request status changed.';
        res.status(200).json(response);
    }).catch(function (err) {
        response.status.code = "500";
        response.status.message = "Request status change failed. Please try again.";
        res.status(response.status.code).json(response);
    })
}


function sendTanRequests(req, res) {
    var response = new Response();
    appService.sendTanRequests(req.body).then(function (data) {
        response.data = data;
        response.status.statusCode = '200';
        response.status.message = 'Request to check tan ';
        res.status(200).json(response);
    }).catch(function (err) {
        logger.info(err)
        response.status.code = "500";
        response.status.message = "Request to check tan  failed. Please try again.";
        res.status(response.status.code).json(response);
    })
}


module.exports = appControler;
