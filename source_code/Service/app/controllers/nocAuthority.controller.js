var log4js = require('log4js');
var logger = log4js.getLogger('NOCA Controller');
var Response = require("../util/response");
var nocAuthorityService = require("../services/nocAuthority.service");

var nocAuthorityController = {
    getRequests: getRequests,
    submitAction: submitAction
  };
  
  //Getting all documents for particular merchant
  
  function getRequests(req, res) {
    //console.log(req);
    let response = new Response();
    nocAuthorityService.getRequests(req)
    .then((requests) => {
      response.data = requests;
      response.status.statusCode = '200';
      response.status.message = 'Requests fetched successfully!! ';
      res.status(200).json(response);
    })
    .catch((err) => {
      response.err = err;
      response.status.statusCode = '500';
      response.status.message = 'Error in fecting requests!! ';
      res.status(500).json(response);
    });
  }
  
  function submitAction(req, res) {
    var response = new Response();
    nocAuthorityService.submitAction(req).then(function (result) {
      response.data = result;
      response.status.statusCode = '200';
      response.status.message = 'Action submitted successfully!! ';
      res.status(200).json(response);
    }).catch(function (err) {
        response.status.code = "500";
        response.status.message = "action submission failed. Please try again.";
        res.status(response.status.code).json(response);
      
    })
  }
  
  module.exports = nocAuthorityController;