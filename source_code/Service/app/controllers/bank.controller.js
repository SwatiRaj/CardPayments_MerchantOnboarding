const bankService = require('../services/bank.service');
// const logger = require('../../config/logger');
var log4js = require('log4js');
var logger = log4js.getLogger('Bank Controller');
const Response = require('../util/response');

const bankController = {
    // getBanks : getBanks,
    setRules : setRules,
    getRequests : getRequests,
    getCategory : getCategory,
    getDocList : getDocList,
    updateStatus: updateStatus
}

// function getBanks(req, res) {
//     let response = new Response();
//     bankService.getBanks(req)
//         .then((banksList) => {
//             response.data.bank = banksList;
//             response.status.statusCode = '200';
//             response.status.message = 'Banks list data fecthed sucessfully!! ';
//             res.status(200).json(response);

//         })
//         .catch((err) => {
//                 response.err = err;
//                 response.status.statusCode = '500';
//                 response.status.message = 'Facing error in getting banks list data!! ';
//                 res.status(500).json(response);
//         });
// }

function setRules(req, res) {
    let response = new Response();
    bankService.setRules(req)
        .then((rules) => {
            response.data.bank = rules;
            response.status.statusCode = '200';
            response.status.message = 'Bank rules set successfully!! ';
            res.status(200).json(response);

        })
        .catch((err) => {
            // if (err.code == 404) {
            //     response.err = err;
            //     response.status.statusCode = '404';
            //     response.status.message = 'Data not found';
            //     res.status(500).json(response);
            // } else {
            console.log(err);
            response.err = err;
            response.status.statusCode = '500';
            response.status.message = 'Facing error in setting bank rules!! ';
            res.status(500).json(response);
            //}
        });
}

//Fetch Approved request details
function getRequests(req, res) {
    let response = new Response();
    bankService.getRequests(req)
        .then((requests) => {
            response.data.bank = requests;
            response.status.statusCode = '200';
            response.status.message = 'Request data fecthed sucessfully!! ';
            res.status(200).json(response);

        })
        .catch((err) => {
            // if (err.code == 404) {
            //     response.err = err;
            //     response.status.statusCode = '404';
            //     response.status.message = 'Data not found';
            //     res.status(500).json(response);
            // } else {
            console.log(err);
            response.err = err;
            response.status.statusCode = '500';
            response.status.message = 'Facing error in getting approved request data!! ';
            res.status(500).json(response);
            //}
        });
}


function getCategory(req, res) {
    let response = new Response();
    bankService.getCategory()
        .then((data) => {
            response.data.category = data;
            response.status.statusCode = '200';
            response.status.message = 'Category fecthed sucessfully!! ';
            res.status(200).json(response);

        })
        .catch((err) => {
            response.err = err;
            response.status.statusCode = '500';
            response.status.message = 'Facing error in getting category data!! ';
            res.status(500).json(response);
        });
}

function getDocList(req, res) {
    let response = new Response();
    bankService.getDocList()
        .then((data) => {
            response.data.bank = data;
            response.status.statusCode = '200';
            response.status.message = 'doc list data fecthed sucessfully!! ';
            res.status(200).json(response);

        })
        .catch((err) => {
            response.err = err;
            response.status.statusCode = '500';
            response.status.message = 'Facing error in getting doc list data!! ';
            res.status(500).json(response);
        });
}

function updateStatus(req,res){
    let response = new Response();
    bankService.updateStatus(req)
        .then((data) => {
            response.data.bank = data;
            response.status.statusCode = '200';
            response.status.message = 'Bank update Status sucessfully!! ';
            res.status(200).json(response);

        })
        .catch((err) => {
            response.err = err;
            response.status.statusCode = '500';
            response.status.message = 'Facing error in update bank Status!! ';
            res.status(500).json(response);
        });
}

module.exports = bankController;