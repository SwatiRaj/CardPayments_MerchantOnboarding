var merchantService = require("../services/merchant.service");
var Response = require("../util/response");
// var logger = require("../../config/logger");
var log4js = require('log4js');
var logger = log4js.getLogger('Merchant Controller');
var Promise = require("bluebird");

var merchantController = {
  getRequests: getRequests,
  submitRequest: submitRequest,
  getBankFromLedger: getBankFromLedger,
  getBanks : getBanks,
  getDocForRequest: getDocForRequest,
  updatePayStatus: updatePayStatus
};

//Getting all documents for particular merchant

function getRequests(req, res) {
  //console.log(req);
  let response = new Response();
  merchantService.getRequests(req.user)
  .then((transactions) => {
    response.data = transactions;
    response.status.statusCode = '200';
    response.status.message = 'Documents fetched successfully!! ';
    res.status(200).json(response);
  })
  .catch((err) => {
    response.err = err;
    response.status.statusCode = '500';
    response.status.message = 'Error in fecting Documents!! ';
    res.status(400).json(response);
  })
}

function submitRequest(req, res) {
  var response = new Response();
  merchantService.submitRequest(req).then(function (result) {
    response.data = result;
    response.status.statusCode = '200';
    response.status.message = 'Request submitted successfully!! ';
    res.status(200).json(response);
  }).catch(function (err) {

      response.status.code = "500";
      response.status.message = "request submission failed. Please try again.";
      res.status(response.status.code).json(response);
    
  });
}

function getBankFromLedger(req, res){
  var response = new Response();
   let bankId = req.body.id;
        let bankUniqueId = req.body.bankUniqueId;
  merchantService.getBankFromLedger(bankId, bankUniqueId).then(function (result) {
    response.data = result;
    response.status.statusCode = '200';
    response.status.message = 'get Bank From Ledger successfully!! ';
    res.status(200).json(response);
  }).catch(function (err) {
      console.log(err);
      response.status.code = "500";
      response.status.message = "get Bank From Ledger failed. Please try again.";
      res.status(response.status.code).json(response);
    
  });
}

function getBanks(req,res){
  var response = new Response();
  merchantService.getBanks(req).then(function (result) {
    response.data = result;
    response.status.statusCode = '200';
    response.status.message = 'get Bank successfully!! ';
    res.status(200).json(response);
  }).catch(function (err) {
      console.log(err);
      response.status.code = "500";
      response.status.message = "get Bank failed. Please try again.";
      res.status(response.status.code).json(response);
    
  });
}

function getDocForRequest(req,res){
  var response = new Response();
  merchantService.getDocForRequest(req).then(function (result) {
    response.data = result;
    response.status.statusCode = '200';
    response.status.message = 'get Doc For Request successfully!! ';
    res.status(200).json(response);
  }).catch(function (err) {
      console.log(err);
      response.status.code = "500";
      response.status.message = "get Doc For Request failed. Please try again.";
      res.status(response.status.code).json(response);
    
  });
}

function updatePayStatus(req,res) {
  var response = new Response();
  merchantService.updatePayStatus(req).then(function (result) {
    response.data = result;
    response.status.statusCode = '200';
    response.status.message = 'Update Pay Status successfully!! ';
    res.status(200).json(response);
  }).catch(function (err) {
      console.log(err);
      response.status.code = "500";
      response.status.message = "Update Pay Status failed. Please try again.";
      res.status(response.status.code).json(response);
    
  });
}

module.exports = merchantController;
