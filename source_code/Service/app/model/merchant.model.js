var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var merchantSchema = new Schema({
    "uniqueId": {
        type: String,
        required: true,
        unique: true
    },
    "password": {
        type: String,
        required: true
    },
    "name": {
        type: String,
        required: true,
    },
    "category": {
        type: String,
        required: true,
    },

    "role": {
        type: String,
        required: true
    },
    "dateOfEstablishment": {
        type: Date,
        required: true
    },
    "emailId": {
        type: String,
        required: true
    },
    "status": {
        type: Boolean,
        required: true
    }
}, {
        timestamps: {
            createdAt: 'createdAt',
            updatedAt: 'updatedAt'
        }
    });

var Merchant = mongoose.model('Merchant', merchantSchema);

module.exports = Merchant;
