var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var organizationSchema = new Schema({
    "uniqueId": {
        type: String,
        required: true,
        unique: true
    },
    "password": {
        type: String,
        required: true
    },
    "name": {
        type: String,
        required: true,
    },

    "role": {
        type: String,
        required: true
    },
     "domainName": {
        type: String,
        required: true
    }
}, {
        timestamps: {
            createdAt: 'createdAt',
            updatedAt: 'updatedAt'
        }
    });

var Organization = mongoose.model('Organization', organizationSchema);

module.exports = Organization;
