const Promise = require('bluebird');
var organizationDao = require('../dao/organization.dao');
var bankMapper = require('../mapper/bank.mapper');
const queryChaincode = require('../hfService/query');
const invokeChaincode = require('../hfService/invokeTransaction');
const hfc = require('fabric-client');
const request = require('request');
var log4js = require('log4js');
const fs = require('fs');
var logger = log4js.getLogger('Bank service');
const merchantService = require('../services/merchant.service');
const merchantDao = require('../dao/merchant.dao');
const constant = require('../util/constants.json');

const bankService = {
    // getBanks: getBanks,
    setRules: setRules,
    getRequests: getRequests,
    getCategory: getCategory,
    getDocList: getDocList,
    updateStatus: updateStatus
};

function setRules(rulesData) {
    return new Promise(async (resolve, reject) => {
        let org = rulesData.user;
        let networkConfig = JSON.parse(fs.readFileSync('artifacts/networkConfig/networkConfig.json'));
        // Load application admin
        //change hereeeeeeeeeeeeeeeee
     
        let channelName = hfc.getConfigSetting('channelName');
        let chaincodeName = hfc.getConfigSetting('chaincodeName');

        var orgforPeers = ((networkConfig).channels[channelName].organizations).toString().split(":")[0];
        let peers = networkConfig.organizations[orgforPeers].peers;

        let fcn = "setBank";
        let args = [org._id.toString(), rulesData.body.category.length.toString()];
        let userName = hfc.getConfigSetting('admins')[0].username;
        let orgName = org.uniqueId;

        for (let i = 0; i < rulesData.body.category.length; i++) {
            args.push(Buffer.from(JSON.stringify(rulesData.body.category[i])));
        }
        // console.log("perrs: ", peers);
        // console.log("channel name ", channelName);
        // console.log("chaincode Name ", chaincodeName);
        // console.log("fun ", fcn);
        // console.log("orgName ", orgName);
        // console.log("userName ", userName);
        // console.log("args ", args);

        await invokeChaincode.invokeChaincode(peers, channelName, chaincodeName, fcn, args, userName, orgName)
            .then((result) => {
                resolve(result)
            }).catch((err) => {
                console.log(err);
                reject(err);
            });
        // resolve(rulesData.body);
    });
}

// function getBanks(request) {
//     return new Promise((resolve, reject) => {
//         console.log("request " , request);
//         organizationDao.getAllBanks().then(function (bank) {
//             var bankArray = [];
//             bank.forEach((eachBank) => {
//                 bankArray.push(bankMapper.bankInfo(eachBank))
//             });

//             //get by merchant id from ledger
//             console.log(request.user._id);
//             resolve(bankArray)
//         }
//         ).catch(function (error) {
//             reject(error)
//         });
//     });
// }

function getRequests(request) {
    return new Promise(async (resolve, reject) => {
        let org = request.user;
        let networkConfig = JSON.parse(fs.readFileSync('artifacts/networkConfig/networkConfig.json'));
        // Load application admin
        let peers = networkConfig.organizations[org.uniqueId].peers;
        let channelName = hfc.getConfigSetting('channelName');
        let chaincodeName = hfc.getConfigSetting('chaincodeName');
        let fcn = "getBankDoc";
        let args = [org.uniqueId];
        let userName = hfc.getConfigSetting('admins')[0].username;
        let orgName = org.uniqueId;
        console.log("perrs: ", peers);
        console.log("channel name ", channelName);
        console.log("chaincode Name ", chaincodeName);
        console.log("fun ", fcn);
        console.log("orgName ", orgName);
        console.log("userName ", userName);
        console.log("args ", args);
        let bankDetail = [];

        await queryChaincode.queryChaincode(peers, channelName, chaincodeName, fcn, args, userName, orgName)
            .then(async (banks) => {
                let bankData = JSON.parse(banks.result);
                console.log(bankData);
                if (bankData != null) {
                    for (let i = 0; i < bankData.length; i++) {
                        let docksArray = [];
                        if (bankData[i].MerchantId != '') {
                            await merchantDao.findMerchantById(bankData[i].MerchantId)
                                .then(async (merchant) => {
                                    let data = await merchantService.getRequests(merchant);
                                    console.log("data: ", data);
                                    for (let i = 0; i < data.length; i++) {
                                        if (data[i].Type == orgName) {
                                            await docksArray.push(data[i]);
                                        }
                                    }
                                    console.log(docksArray);
                                    let merchantObject = {
                                        name: '',
                                        dockList: [],
                                        DOE: ''
                                    }
                                    merchantObject.name = merchant.name;
                                    merchantObject.DOE = merchant.dateOfEstablishment;
                                    merchantObject.dockList = docksArray;

                                    await bankDetail.push(merchantObject)
                                });
                        }
                    }
                    resolve(bankDetail);
                }
                else {
                    resolve([]);
                }

            }).catch(err => {
                console.log(err);
                reject(err);
            });
    });
}

function getCategory() {
    return new Promise((resolve, reject) => {
        resolve(constant.CATEGORIES);
    });
}

function getDocList() {
    return new Promise((resolve, reject) => {
        resolve(constant.DOCUMENTLIST);
    });
}

function updateStatus(request) {
    return new Promise(async (resolve, reject) => {
        let org = request.user;
        let body = request.body;
        console.log("bank: ", org);
        console.log("body: ", body);
        let networkConfig = JSON.parse(fs.readFileSync('artifacts/networkConfig/networkConfig.json'));
        // Load application admin
        //change hereeeeeeeeeeeeeeeee

        let channelName = hfc.getConfigSetting('channelName');

        var orgforPeers = ((networkConfig).channels[channelName].organizations).toString().split(":")[0];
        let peers = networkConfig.organizations[orgforPeers].peers;

        let chaincodeName = hfc.getConfigSetting('chaincodeName');
        let fcn = "updateBankDocStatus";
        let args = [org.uniqueId, body.merchantId.toString(), body.status, body.paymentStatus];
        let userName = hfc.getConfigSetting('admins')[0].username;
        let orgName = org.uniqueId;

        // console.log("perrs: ", peers);
        // console.log("channel name ", channelName);
        // console.log("chaincode Name ", chaincodeName);
        // console.log("fun ", fcn);
        // console.log("orgName ", orgName);
        // console.log("userName ", userName);
        // console.log("args ", args);

        let response = await invokeChaincode.invokeChaincode(peers, channelName, chaincodeName, fcn, args, userName, orgName);

        resolve(response);
    });
}

module.exports = bankService;