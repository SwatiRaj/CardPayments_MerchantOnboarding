var Promise = require('bluebird');
var log4js = require('log4js');
var logger = log4js.getLogger('Network service');
var shell = require('shelljs');
var hfc = require('fabric-client');
var fs = require('fs');
var scriptDir = "./artifacts/scripts/";
var Admin = require("../hfService/admin")
var createChannel = require('../hfService/createChannel');
var consortium = require('../hfService/consortium');
var joinChannel = require('../hfService/joinChannel');
var installChaincode = require('../hfService/installChaincode');
var instantiateChaincode = require('../hfService/instantiateChaincode');
var upgradeChaincode = require("../hfService/upgradeChaincode");
var invokeChaincode = require('../hfService/invokeTransaction');
var updateChannel = require('../hfService/updateChannel');
var hfc = require('fabric-client');
var scriptDir = "./artifacts/scripts/";
var helper = require("../hfService/helper");
var query = require('../hfService/query')
var jwt = require('jsonwebtoken');
var jwtConfig = require("../../config/jwtConfig");
var userRegister = require("../hfService/user")


var networkService = {
    login: login,
    createChannel: createChannelService,
    joinChannel: joinChannelService,
    upgradeChaincode: upgradeChaincodeService,
    invokeChaincode: invokeChaincodeService,
    upgradePrivateChaincode: upgradePrivateChaincodeService
};

function login(adminObj) {
    return new Promise(async function (resolve, reject) {
        var result = await Admin.getAdmin(adminObj.org)
        if (result.status == "ERROR") {
            reject(result)
        }
        var admins = hfc.getConfigSetting('admins');
        if (adminObj.email == admins[0].username && adminObj.password == admins[0].secret) {
            var payload = {
                email: adminObj.email,
                org: adminObj.org,
                role: 'Admin'
            };
            var token = jwt.sign(payload, jwtConfig.jwtSecret, {
                expiresIn: jwtConfig.expireTime
            });
            resolve(token);
        }
        else {
            reject({ code: 401 });
        }
    });
}

function createChannelService(org) {
    return new Promise(async function (resolve, reject) {
        var networkConfig = JSON.parse(fs.readFileSync('artifacts/networkConfig/networkConfig.json'));
        var userName = hfc.getConfigSetting('admins')[0].username;
        var channelName = hfc.getConfigSetting('channelName');
        var consortiumName = hfc.getConfigSetting('consortiumName');
        var orderer = networkConfig.defaultOrderer;
        var chaincodeName = hfc.getConfigSetting('chaincodeName');
        var chaincodeVersion = hfc.getConfigSetting('chaincodeVersion');
        var chaincodePath = hfc.getConfigSetting('chaincodePath');

        var consortiumResult = await consortium.createConsortium(consortiumName, [org], orderer, org, userName)
        if (consortiumResult.status == "ERROR") {
            reject(consortiumResult.error)
        }

        shell.exec(scriptDir + "createChannel.sh " + channelName + " " + consortiumName + " " + orderer + " " + chaincodeName + " " + chaincodeVersion + " " + org);

        var consortiumCreated = false
        console.log("************************ Consortium check ************************ ")
        while (consortiumCreated == false) {
            var result = await consortium.checkConsortium(consortiumName, orderer, org, userName)
            if (result.status == "NOT_FOUND") {
                for (var i = 0; i < 1000000000; i++);
            } else if (result.status == "FOUND") {
                consortiumCreated = true
            }
            else reject(result.error)
        }
        console.log("************************ Consortium check completed ************************ ")

        var channelResult = await createChannel.createChannel(channelName, org, userName)
        if (channelResult.status == "ERROR") {
            reject(channelResult.error)
        }

        // Check whether the channel has been created or not before joining peers
        var channelCreated = false
        while (channelCreated == false) {
            var client = await helper.getClientForOrg(org, userName);

            var channel = client.getChannel(channelName);

            try {
                await channel.getChannelConfigFromOrderer();
                channelCreated = true;
                logger.info("#################################### CHANNEL CREATED ####################################")

            } catch (error) {
                logger.info("#################################### CHANNEL NOT CREATED ####################################")
                for (var i = 0; i < 1000000000; i++);
            }
        }
        // END

        var peers = networkConfig.organizations[org].peers;

        // Join channel
        var joinResult = await joinChannel.joinChannel(channelName, peers, org, userName)
        if (joinResult.status == "ERROR") {
            reject(joinResult.error)
        }

        // Install chain code
        var installCommonChaincodeResult = await installChaincode.installChaincode(peers, chaincodeName, chaincodeVersion, chaincodePath, org, userName)
        if (installCommonChaincodeResult.status == "ERROR") {
            reject(installCommonChaincodeResult.error)
        }

        var fcn = "";
        var peers = networkConfig.organizations[org].peers;
        var argsForInit = []

        var initiateCommonChaincodeResult = await instantiateChaincode.instantiateChaincode(peers, channelName, chaincodeName, chaincodeVersion, fcn, argsForInit, org, userName)
        if (initiateCommonChaincodeResult.status == "ERROR") {
            reject(initiateCommonChaincodeResult.error)
        }

        resolve(channelName);

    });
}


function invokeChaincodeService(org) {
    return new Promise(async function (resolve, reject) {
        var networkConfig = JSON.parse(fs.readFileSync('artifacts/networkConfig/networkConfig.json'));
        var userName = hfc.getConfigSetting('admins')[0].username;
        var channelName = hfc.getConfigSetting('channelName');
        var chaincodeName = hfc.getConfigSetting('chaincodeName');

        await userRegister.registerAndEnrollUser("Pranith", org)
        // var peers = networkConfig.organizations[org].peers;
        // logger.info("Peers =============== > ")
        // logger.info(peers)
        // //Dummy invoke
        // var fcn = "dummy";
        // var args = []
        // var invokeResult = await invokeChaincode.invokeChaincode(peers, channelName, chaincodeName, fcn, args,
        //     userName, org);
        // if (invokeResult.status == "ERROR") {
        //     reject(invokeResult.error)
        // }
        resolve(channelName);
    });
}

function joinChannelService(org) {
    return new Promise(async function (resolve, reject) {
        var networkConfig = JSON.parse(fs.readFileSync('artifacts/networkConfig/networkConfig.json'));
        var userName = hfc.getConfigSetting('admins')[0].username;
        var channelName = hfc.getConfigSetting('channelName');
        var chaincodeNameAndVersion = (networkConfig.channels[channelName]["chaincode"]).toString().split(":");
        var chaincodeName = chaincodeNameAndVersion[0];
        var chaincodeVersion = chaincodeNameAndVersion[1];

        var chaincodePath = hfc.getConfigSetting('chaincodePath');
        var peers = networkConfig.organizations[org].peers;

        logger.info("Chain code =============== > ")
        logger.info(chaincodeName, chaincodeVersion)

        // Join channel
        var joinResult = await joinChannel.joinChannel(channelName, peers, org, userName)
        if (joinResult.status == "ERROR") {
            reject(joinResult.error)
        }

        // Install chain code

        var installCommonChaincodeResult = await installChaincode.installChaincode(peers, chaincodeName, chaincodeVersion, chaincodePath, org, userName)
        if (installCommonChaincodeResult.status == "ERROR") {
            reject(installCommonChaincodeResult.error)
        }

        // Check whether the peers has joined the channel
        var chaincodeName = networkConfig.channels[channelName].chaincode.split(":")[0];
        var fcn = "dummy";
        var args = []

        var joined = false
        while (joined == false) {
            try {
                // Dummy invoke

                var invokeResult = await invokeChaincode.invokeChaincode(peers, channelName, chaincodeName, fcn, args,
                    userName, org);
                if (invokeResult.status == "ERROR") {
                    reject(invokeResult.error)
                } joined = true;
                logger.info("#################################### Joined CREATED ####################################")
            } catch (error) {
                logger.info("#################################### Not Joined ####################################")
                for (var i = 0; i < 1000000000; i++);
            }
        }
        // END
        resolve("Successfull");
    });
}

function upgradeChaincodeService() {
    return new Promise(async function (resolve, reject) {

        var networkConfig = JSON.parse(fs.readFileSync('artifacts/networkConfig/networkConfig.json'));
        var userName = hfc.getConfigSetting('admins')[0].username;
        var channelName = hfc.getConfigSetting('channelName');
        var chaincodePath = hfc.getConfigSetting('chaincodePath');

        var chaincodeNameAndVersion = (networkConfig.channels[channelName]["chaincode"]).toString().split(":");
        var chaincodeName = chaincodeNameAndVersion[0];

        // should increase the version no
        var chaincodeVersion = ((parseFloat(chaincodeNameAndVersion[1]) + parseFloat('0.1')).toFixed(1)).toString();

        shell.exec(scriptDir + "updateChaincode.sh " + channelName + " " + chaincodeName + " " + chaincodeVersion);

        var orgs = ((networkConfig).channels[channelName].organizations).toString().split(":")
        for (var i = 0; i < orgs.length; i++) {
            var peers = networkConfig.organizations[orgs[i]].peers;
            // Install chain code
            var installResult = await installChaincode.installChaincode(peers, chaincodeName, chaincodeVersion, chaincodePath, orgs[i], userName)
            if (installResult.status == "ERROR") {
                reject(installResult.error)
            }
        }

        var fcn = "";
        var args = ["upgrade"]
        // // Initiate chaincode on all peers
        // var peers = []
        // for (var i = 0; i < orgs.length; i++)
        //     peers = peers.concat(networkConfig.organizations[orgs[i]].peers)

        // Invoke on one peer for testing
        var peers = networkConfig.organizations[orgs[0]].peers
        var upgradeChanicodeResult = await upgradeChaincode.upgradeChaincode(peers, channelName, chaincodeName, chaincodeVersion, fcn, args, orgs[0], userName)

        if (upgradeChanicodeResult.status == "ERROR") {
            reject(upgradeChanicodeResult.error)
        }
        resolve("Successfully updated chaincode on the channel " + channelName);

    });
}


function upgradePrivateChaincodeService(channelName) {
    return new Promise(async function (resolve, reject) {
        var networkConfig = JSON.parse(fs.readFileSync('artifacts/networkConfig/networkConfig.json'));
        var appAdmin = networkConfig.appAdmin;
        var userName = hfc.getConfigSetting('admins')[0].username;
        var chaincodeNameAndVersion = (networkConfig.channels[channelName]["privateChaincode"]).toString().split(":");
        var chaincodeName = chaincodeNameAndVersion[0];
        var chaincodePath = "bank";

        // should increase the version no
        var chaincodeVersion = ((parseFloat(chaincodeNameAndVersion[1]) + parseFloat('0.1')).toFixed(1)).toString();

        var orgs = ((networkConfig).channels[channelName].organizations).toString().split(":")
        for (var i = 0; i < orgs.length && orgs[i] != appAdmin; i++) {

            var peers = networkConfig.organizations[orgs[i]].peers;

            // Install chain code
            var installResult = await installChaincode.installChaincode(peers, chaincodeName, chaincodeVersion, chaincodePath, orgs[i], userName)
            if (installResult.status == "ERROR") {
                reject(installResult.error)
            }

        }
        var fcn = "";
        var length = 4
        var args = ["upgrade"]

        // Initiate chaincode on all peers
        var peers = []
        for (var i = 0; i < orgs.length && orgs[i] != appAdmin; i++)
            peers = peers.concat(networkConfig.organizations[orgs[i]].peers)

        // Initiate chaincode on admin peers
        // var peers = networkConfig.organizations[appAdmin].peers[0];
        var upgradeChanicodeResult = await upgradeChaincode.upgradeChaincode(peers, channelName, chaincodeName, chaincodeVersion, fcn, args, appAdmin, userName)

        if (upgradeChanicodeResult.status == "ERROR") {
            reject(upgradeChanicodeResult.error)
        }
        shell.exec(scriptDir + "updateChaincode.sh " + channelName + " " + chaincodeName + " " + chaincodeVersion);
        resolve("Successfully updated chaincode on the channel " + channelName);

    });
}
module.exports = networkService;

