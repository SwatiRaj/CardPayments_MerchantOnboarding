// var logger = require("../../config/logger");
var log4js = require('log4js');
var logger = log4js.getLogger('merchant service');
var Promise = require('bluebird');
var mongoose = require('mongoose');
var merchantDao = require('../dao/merchant.dao');
var userMapper = require('../mapper/user.mapper')
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var jwtConfig = require("../../config/jwtConfig");
const saltRounds = 10;

const request = require('request');
const fs = require('fs');
const hfc = require('fabric-client');

const userEnrollment = require('../hfService/user');
const queryChaincode = require('../hfService/query');
const invokeChaincode = require('../hfService/invokeTransaction');

const organizationDao = require('../dao/organization.dao');
const bankMapper = require('../mapper/bank.mapper');

const merchantService = {
    getRequests: getRequests,
    submitRequest: submitRequest,
    getBankFromLedger: getBankFromLedger,
    getBanks: getBanks,
    getDocForRequest: getDocForRequest,
    updatePayStatus: updatePayStatus
};

function getRequests(user) {
    return new Promise(async (resolve, reject) => {

        const networkConfig = JSON.parse(fs.readFileSync('artifacts/networkConfig/networkConfig.json'));

        let userName = "admin";

        // let peerNames = networkConfig.organizations[orgName].peers;
        let channelName = hfc.getConfigSetting("channelName");
        let chaincodeName = hfc.getConfigSetting("chaincodeName");

        var orgName = ((networkConfig).channels[channelName].organizations).toString().split(":")[0];
        let peerNames = networkConfig.organizations[orgName].peers;

        let fcn = "getMerchantRequests";
        let args = [user._id.toString()];
        let response = await queryChaincode.queryChaincode(peerNames, channelName, chaincodeName, fcn, args, userName, orgName);
        response = await JSON.parse(response.result);
        // console.log(response)
        if (response != null) {
            let responseArray = [];
            for (var key in response) await responseArray.push(response[key].Type);

            let resultArray = [];
            for (let i = 0; i < response.length; i++) {
                let result = await organizationDao.getBankByName({ uniqueId: response[i].Type, role: "Bank" })
                    .then(async (organization) => {
                        if (organization != null) {
                            let fcn = "getBank";
                            let args = [organization._id.toString()];
                            return await queryChaincode.queryChaincode(peerNames, channelName, chaincodeName, fcn, args, userName, orgName)
                        }
                        else {
                            return null;
                        }
                    })
                    .catch((err) => reject(err));
                if (result != null && result.result.length > 0) {
                    let categories = await JSON.parse(result.result).CategoryList
                    for (let j = 0; j < categories.length; j++) {
                        if (categories[j].Name == user.category) {
                            response[i].docs = [];
                            for (let k = 0; k < categories[j].Doclist.length; k++) {
                                let index = await responseArray.indexOf(categories[j].Doclist[k]);
                                if (index > -1) {
                                    await response[i].docs.push(response[index]);
                                }
                            }
                            await resultArray.push(response[i]);

                            break;
                        }
                    }
                }
            }


            resolve(resultArray);
        }
        else
            resolve([]);
    });
}


function submitRequest(requestObject) {
    return new Promise(async (resolve, reject) => {
        let requestData = requestObject.body;

        const networkConfig = JSON.parse(fs.readFileSync('artifacts/networkConfig/networkConfig.json'));

        // let peerNames = [];

        // // for (var key in networkConfig.channels[channelName].peers) await peerNames.push(key);
        // await peerNames.push("peer0.bankone.com");



        let channelName = hfc.getConfigSetting("channelName");
        let chaincodeName = hfc.getConfigSetting("chaincodeName");
        let fcn = "sendRequest";
        let userName = "admin";
        let orgName = requestData.orgName;

        var orgforPeers = ((networkConfig).channels[channelName].organizations).toString().split(":")[0];
        let peerNames = networkConfig.organizations[orgforPeers].peers;

        let args = [requestObject.user.name.toString(), requestObject.user.emailId,
        requestObject.user.dateOfEstablishment.toString(),
        requestObject.user.category, requestObject.user._id.toString(), orgName,
        requestData.amount.toString(), requestData.documents.length.toString()]

        for (let i = 0; i < requestData.documents.length; i++) {
            await args.push(Buffer.from(JSON.stringify(requestData.documents[i])));
            if (requestData.documents[i].type == "Document3") {
                logger.info("Calling External service")
                var externalServiceData = {
                    merchantId: requestObject.user._id.toString(),
                    type: requestData.documents[i].type
                }
                request.post("http://localhost:51099/tan-services/apis/checktan",
                    { form:  externalServiceData  }, (err, res, body) => {
                        logger.info("Requested to check tan");
                    });
            }
        }

        let response = await invokeChaincode.invokeChaincode(peerNames, channelName, chaincodeName, fcn, args, userName, orgName);

        resolve(response);

        // resolve("Done")
    });
}


function getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var ageDifMs = today.getTime() - birthDate.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

function getBankFromLedger(bankId, bankUniqueId, merchantCategory, merchantId) {
    return new Promise(async (resolve, reject) => {
        // let bankId = request.body.id;
        // let bankUniqueId = request.body.bankUniqueId;

        let networkConfig = JSON.parse(fs.readFileSync('artifacts/networkConfig/networkConfig.json'));
        // Load application admin
        let peers = networkConfig.organizations[bankUniqueId].peers;
        let channelName = hfc.getConfigSetting('channelName');
        let chaincodeName = hfc.getConfigSetting('chaincodeName');
        let fcn = "getBankDocumentsForRequest";
        let args = [bankId, merchantCategory, merchantId];
        let userName = hfc.getConfigSetting('admins')[0].username;
        let orgName = bankUniqueId;
        // console.log("perrs: ", peers);
        // console.log("channel name ", channelName);
        // console.log("chaincode Name ", chaincodeName);
        // console.log("fun ", fcn);
        // console.log("orgName ", orgName);
        // console.log("userName ", userName);
        // console.log("args ", args);
        var response = await queryChaincode.queryChaincode(peers, channelName, chaincodeName, fcn, args, userName, orgName);
        resolve(JSON.parse(response.result));
    });
}

function getBanks(request) {
    return new Promise((resolve, reject) => {
        organizationDao.getAllBanks().then(async (bank) => {
            var bankArray = [];
            var responseBankArray = []
            bank.forEach((eachBank) => {
                bankArray.push(bankMapper.bankInfo(eachBank))
            });
            //get by merchant id from ledger
            console.log(request.user);
            let merchantrequest = await getRequests(request.user);
            if (merchantrequest.length <= 0) {
                resolve(bankArray);
            }
            else {
                console.log("merchant request: ", merchantrequest);
                for (let i = 0; i < bankArray.length; i++) {
                    var found = false
                    for (let j = 0; j < merchantrequest.length; j++) {
                        console.log(bankArray[i].bankUniqueId, merchantrequest[j].Type)
                        console.log(bankArray[i].bankUniqueId == merchantrequest[j].Type)
                        if (bankArray[i].bankUniqueId == merchantrequest[j].Type && merchantrequest[j].Status != "REJECTED") {
                            found = true
                        }
                    }
                    if (!found) {
                        responseBankArray.push(bankArray[i])
                    }
                }
            }
            resolve(responseBankArray);
        }
        ).catch(function (error) {
            reject(error)
        });
    });
}

function getDocForRequest(request) {
    return new Promise(async (resolve, reject) => {
        let body = request.body;
        let merchant = request.user;
        console.log(request.user);

        var bankDetail = await getBankFromLedger(body.id, body.bankUniqueId, merchant.category, merchant._id.toString());
        resolve(bankDetail);
    });
}

function updatePayStatus(request) {
    return new Promise(async (resolve, reject) => {
        let merchant = request.user;
        let body = request.body;

        let networkConfig = JSON.parse(fs.readFileSync('artifacts/networkConfig/networkConfig.json'));
        // Load application admin
        //change hereeeeeeeeeeeeeeeee



        let channelName = hfc.getConfigSetting('channelName');
        let chaincodeName = hfc.getConfigSetting('chaincodeName');

        var orgforPeers = ((networkConfig).channels[channelName].organizations).toString().split(":")[0];
        let peers = networkConfig.organizations[orgforPeers].peers;


        let fcn = "updateBankDocStatus";
        let args = [body.bankUniqueId, merchant._id.toString(), body.status, body.paymentStatus];
        let userName = hfc.getConfigSetting('admins')[0].username;
        let orgName = body.bankUniqueId;

        let result = await invokeChaincode.invokeChaincode(peers, channelName, chaincodeName, fcn, args, userName, orgName)
        resolve(result);

    });
}

module.exports = merchantService;
