var log4js = require('log4js');
var logger = log4js.getLogger('NOCA service');
const fs = require('fs');
const hfc = require('fabric-client');
const queryChaincode = require('../hfService/query');
const invokeChaincode = require('../hfService/invokeTransaction');
const merchantDao = require('../dao/merchant.dao');

var nocAuthorityService = {
    getRequests: getRequests,
    submitAction: submitAction
}

function getRequests(request) {
    return new Promise(async (resolve, reject) => {
        let org = request.user;
        let networkConfig = JSON.parse(fs.readFileSync('artifacts/networkConfig/networkConfig.json'));
        // Load application admin
        let peers = networkConfig.organizations[org.uniqueId].peers;
        let channelName = hfc.getConfigSetting('channelName');
        let chaincodeName = hfc.getConfigSetting('chaincodeName');
        let fcn = "getDoc";

        let args = [];
        if (org.uniqueId == "NOCA1") args.push("Document1");
        else if (org.uniqueId == "NOCA2") args.push("Document2");
        else if (org.uniqueId == "NOCA3") args.push("Documwnt3");
        else if (org.uniqueId == "NOCA4") args.push("Documwnt4");

        let userName = hfc.getConfigSetting('admins')[0].username;
        let orgName = org.uniqueId;

        let response = await queryChaincode.queryChaincode(peers, channelName, chaincodeName, fcn, args, userName, orgName);
        response = JSON.parse(response.result);
        if (response == null) {
            reject({ message: "No requests" });
        }
        let requestArray = [];

        for (let i = 0; i < response.length; i++) {
            console.log(response[i].MerchantId);

            await merchantDao.findMerchantById(response[i].MerchantId)
                .then(async (merchant) => {
                    let object = {
                        merchantName: "",
                        DOE: "",
                        document: {}
                    }
                    object.merchantName = merchant.name,
                        object.DOE = merchant.dateOfEstablishment,
                        object.document = response[i];
                    await requestArray.push(object);
                })
                .catch((err) => {
                    console.log(err);
                    reject(err);
                });
        }
        console.log(requestArray);
        resolve(requestArray);
    });
}

function submitAction(request) {
    return new Promise(async (resolve, reject) => {
        let org = request.user;
        let body = request.body;
        let networkConfig = JSON.parse(fs.readFileSync('artifacts/networkConfig/networkConfig.json'));
        // Load application admin
        //change hereeeeeeeeeeeeeeeeeeeeeeeee
       
        let channelName = hfc.getConfigSetting('channelName');

        
        var orgforPeers = ((networkConfig).channels[channelName].organizations).toString().split(":")[0];
        let peers = networkConfig.organizations[orgforPeers].peers;


        let chaincodeName = hfc.getConfigSetting('chaincodeName');
        let fcn = "updateDocStatus";
        let args = ["", body.merchantId, body.status];
        if (org.uniqueId == "NOCA1") args[0] = "Document1";
        else if (org.uniqueId == "NOCA2") args[0] = "Document2";
        else if (org.uniqueId == "NOCA3") args[0] = "Document3";
        else if (org.uniqueId == "NOCA4") args[0] = "Document4";

        let userName = hfc.getConfigSetting('admins')[0].username;
        let orgName = org.uniqueId;

        console.log("peers: ", peers);
        console.log("channel name ", channelName);
        console.log("chaincode Name ", chaincodeName);
        console.log("fun ", fcn);
        console.log("orgName ", orgName);
        console.log("userName ", userName);
        console.log("args ", args);

        let response = await invokeChaincode.invokeChaincode(peers, channelName, chaincodeName, fcn, args, userName, orgName);
        resolve(response);
    });
}


module.exports = nocAuthorityService;



