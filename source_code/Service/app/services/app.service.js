// var logger = require("../../config/logger");
var log4js = require('log4js');
var logger = log4js.getLogger('App service');
var Promise = require('bluebird');
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var jwtConfig = require("../../config/jwtConfig");
const saltRounds = 10;
var merchantDao = require("../dao/merchant.dao")
var organizationDao = require("../dao/organization.dao")
var hfc = require('fabric-client');
var shell = require('shelljs');
var scriptDir = "./artifacts/scripts/";
var hfcConfig = require("../../config/hfcConfig");
var networkService = require("../services/network.service")
var Admin = require("../hfService/admin");
var updateChannel = require("../hfService/updateChannel")
const invokeChaincode = require('../hfService/invokeTransaction');

var fs = require("fs")
const request = require('request');


const appService = {
    signup: signup,
    login: login,
    changeRequestStatus: changeRequestStatus,
    sendTanRequests: sendTanRequests,
    getInfo: getInfo
};

function signup(registerObject) {
    return new Promise(function (resolve, reject) {
        if (registerObject.role == "Merchant") {
            var Merchant = {
                "uniqueId": registerObject.uniqueId,
                "name": registerObject.fullName,
                "password": registerObject.password,
                "category": registerObject.category,
                "emailId": registerObject.emailId,
                "dateOfEstablishment": registerObject.dateOfEstablishment,
                "role": registerObject.role,
                "status": false
            };
            merchantDao.checkDuplicateMerchant(registerObject.uniqueId).then(function () {
                merchantDao.signup(Merchant).then(function (res) {
                    resolve(res);
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        } else if (registerObject.role == "Bank" || registerObject.role == "NOCA") {
            var Organization = {
                "uniqueId": registerObject.uniqueId,
                "name": registerObject.fullName,
                "password": registerObject.password,
                "domainName": registerObject.domainName,
                "role": registerObject.role
            };
            organizationDao.checkDuplicateOrganization(registerObject.uniqueId, registerObject.domainName).then(function () {
                organizationDao.signup(Organization).then(async function (res) {

                    // Execute the scripts to Create organization
                    var noOfPeers = hfc.getConfigSetting("noOfPeers");
                    shell.exec(scriptDir + "createOrganization.sh " + Organization.uniqueId + " " + Organization.domainName + " " + noOfPeers);
                    hfcConfig.loadOrgsProfile();

                    for (var i = 0; i < 200000 * 100000; i++) {

                    }

                    var result = await Admin.getAdmin(Organization.uniqueId)
                    if (result.status == "ERROR") {
                        reject(result)
                    }

                    organizationDao.organizationCount().then(function (count) {
                        if (count == 1) {
                            logger.info("Create new Channel");
                            networkService.createChannel(registerObject.uniqueId).then(function () {
                                resolve();
                            }).catch(function (err) {
                                reject(err)
                            })
                        } else {
                            logger.info("Join Channel");
                            updateChannel.updateChannel(registerObject.uniqueId).then(function () {
                                var networkConfig = JSON.parse(fs.readFileSync('artifacts/networkConfig/networkConfig.json'));
                                var orderer = networkConfig.defaultOrderer;

                                var channelName = hfc.getConfigSetting('channelName');
                                var chaincodeNameAndVersion = (networkConfig.channels[channelName]["chaincode"]).toString().split(":");
                                var chaincodeName = chaincodeNameAndVersion[0];
                                var chaincodeVersion = chaincodeNameAndVersion[1];

                                var orderer = networkConfig.defaultOrderer;
                                var orgs = [];
                                orgs = orgs.concat((networkConfig).channels[channelName].organizations).toString().split(":");
                                orgs.push(registerObject.uniqueId)
                                var orgList = orgs.reduce((prevOrg, currOrg) => prevOrg + " " + currOrg);
                                logger.info(" arguments ===================== > ")
                                logger.info(scriptDir + "updateChannel.sh " + channelName + " " + orderer + " " + chaincodeName + " " + chaincodeVersion + " " + orgs.length + " " + orgList)
                                shell.exec(scriptDir + "updateChannel.sh " + channelName + " " + orderer + " " + chaincodeName + " " + chaincodeVersion + " " + orgs.length + " " + orgList);
                                networkService.joinChannel(registerObject.uniqueId).then(function () {
                                    resolve();
                                }).catch(function (err) {
                                    reject(err)
                                })
                            }).catch(function (err) {
                                reject(err)
                            })
                        }
                    }).catch(function (err) {
                        reject(err);
                    });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        }
        else {
            reject({ code: 404, message: "Role not found" })
        }
    });
}


function login(loginObject) {
    return new Promise(function (resolve, reject) {
        if (loginObject.role == "Merchant") {
            var Merchant = {
                "uniqueId": loginObject.uniqueId,
                "password": loginObject.password,
            };
            merchantDao.login(Merchant).then(function (res) {
                var payload = {
                    id: res._id,
                    role: res.role,
                    uniqueId: res.uniqueId
                };
                var token = jwt.sign(payload, jwtConfig.jwtSecret, {
                    expiresIn: jwtConfig.expireTime
                });
                resolve(token);
            }).catch(function (err) {
                reject(err);
            });

        } else if (loginObject.role == "Bank" || loginObject.role == "NOCA") {
            var Organization = {
                "uniqueId": loginObject.uniqueId,
                "password": loginObject.password,
            };
            organizationDao.login(Organization).then(function (res) {
                var payload = {
                    id: res._id,
                    role: res.role,
                    uniqueId: res.uniqueId
                };
                var token = jwt.sign(payload, jwtConfig.jwtSecret, {
                    expiresIn: jwtConfig.expireTime
                });
                resolve(token);
            }).catch(function (err) {
                reject(err);
            });
        } else {
            reject({ code: 404, message: "Role not found" })
        }
    });
}



function getInfo(user) {
    return new Promise(function (resolve, reject) {
        if (user.role == "Merchant") {

            merchantDao.findMerchantById(user.id).then(function (res) {
                resolve(res);
            }).catch(function (err) {
                reject(err);
            });

        } else if (user.role == "Bank" || user.role == "NOCA") {
            organizationDao.findOrganizationById(user.id).then(function (res) {
                resolve(res);
            }).catch(function (err) {
                reject(err);
            })
        } else {
            reject({ code: 404, message: "Role not found" })
        }
    });
}


function changeRequestStatus(data) {
    return new Promise(async function (resolve, reject) {
        logger.info(data)

       
        let networkConfig = JSON.parse(fs.readFileSync('artifacts/networkConfig/networkConfig.json'));
        // Load application admin
        //change hereeeeeeeeeeeeeeeeeeeeeeeee

        let channelName = hfc.getConfigSetting('channelName');


        var orgName = ((networkConfig).channels[channelName].organizations).toString().split(":")[0];
        let peers = networkConfig.organizations[orgName].peers;


        let chaincodeName = hfc.getConfigSetting('chaincodeName');
        let fcn = "updateDocStatus";
        let args = [data.type, data.merchantId, data.status];


        let userName = hfc.getConfigSetting('admins')[0].username;

        console.log("peers: ", peers);
        console.log("channel name ", channelName);
        console.log("chaincode Name ", chaincodeName);
        console.log("fun ", fcn);
        console.log("orgName ", orgName);
        console.log("userName ", userName);
        console.log("args ", args);

        let response = await invokeChaincode.invokeChaincode(peers, channelName, chaincodeName, fcn, args, userName, orgName);
        resolve(response)
    });
}


function sendTanRequests(data) {
    return new Promise(function (resolve, reject) {
        request.post("http://localhost:51099/tan-services/apis/checktan",
            { form: { data: "data" } }, (err, res, body) => {
                logger.info("Reuest to check tan");
            });
        resolve(data)
    });
}


module.exports = appService;
