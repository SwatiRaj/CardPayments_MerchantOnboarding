var userMapper = {
    userInfo: userInfo
}

function userInfo(userDeatils) {
    return {
        customerAadhar: userDeatils.aadhar,
        customerAccountNo: userDeatils.accountNo,
        customerAddress: userDeatils.address,
        customerContactNo: userDeatils.contactNo,
        customerName: userDeatils.customerName,
        customerDob: userDeatils.dob,
        customerEmail: userDeatils.email,
        customerSsn: userDeatils.ssn,
        customerId: userDeatils._id,
        customerType: userDeatils.customerType
    };
}
module.exports = userMapper;