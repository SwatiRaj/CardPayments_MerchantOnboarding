var bankMapper = {
    bankInfo: bankInfo
}

function bankInfo(bankDetails) {
    return {
        bankUniqueId: bankDetails.uniqueId,
        bankName: bankDetails.name,
        id: bankDetails._id
    };
}
module.exports = bankMapper;