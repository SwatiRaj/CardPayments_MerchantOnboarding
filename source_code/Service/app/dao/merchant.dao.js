var Promise = require("bluebird");
var Merchant = require('../model/merchant.model');
var log4js = require('log4js');
var logger = log4js.getLogger('Merchat Dao');

var merchantDao = {
    signup: signup,
    login: login,
    checkDuplicateMerchant: checkDuplicateMerchant,
    findMerchantById: findMerchantById
};

function signup(merchantObject) {
    var merchant = new Merchant(merchantObject);
    return new Promise(function (resolve, reject) {
        merchant.save(function (err, data) {
            if (!err) {
                resolve(data);
            } else {
                reject(err);
            }
        });
    });
}

function checkDuplicateMerchant(uniqueId) {
    return new Promise(function (resolve, reject) {
        Merchant.findOne({ uniqueId: uniqueId }, function (err, merchant) {
            if (err) {
                reject(err)
            }
            else {
                if (merchant == null) {
                    resolve()
                }
                else {
                    reject({ code: 409 })
                }
            }
        })
    });
}


function login(query) {
    return new Promise(function (resolve, reject) {
        Merchant.findOne(
            query
            , function (err, merchant) {
                if (!err) {
                    if (merchant != null) {
                        resolve(merchant);
                    } else {
                        reject({ code: 401 });
                    }
                } else {
                    reject(err);
                }
            });
    });
};

function findMerchantById(merchantId) {
    return new Promise(function (resolve, reject) {
        Merchant.findById(merchantId, function (err, merchant) {
            if (err) {
                reject(err)
            }
            else {
                if (merchant == null) {
                    reject({ code: 409 })
                }
                else {
                    resolve(merchant)
                }
            }
        })
    });
}


module.exports = merchantDao;
