var Promise = require("bluebird");
var Organization = require('../model/organization.model');
var log4js = require('log4js');
var logger = log4js.getLogger('Organization Dao');

var organizationDao = {
    signup: signup,
    login: login,
    checkDuplicateOrganization: checkDuplicateOrganization,
    findOrganizationById: findOrganizationById,
    organizationCount: organizationCount,
    getAllBanks: getAllBanks,
    getBankByName: getBankByName
};

function organizationCount() {
    return new Promise(function (resolve, reject) {
        Organization.count(function (err, count) {
            if (!err) {
                resolve(count);
            } else {
                reject(err);
            };

        });
    });
}

function signup(organizationObject) {
    var organization = new Organization(organizationObject);
    return new Promise(function (resolve, reject) {
        organization.save(function (err, data) {
            if (!err) {
                resolve(data);
            } else {
                reject(err);
            }
        });
    });
}

function checkDuplicateOrganization(uniqueId, domainName) {
    return new Promise(function (resolve, reject) {
        // id and domain name should be unique
        Organization.findOne({ $or: [{ uniqueId: uniqueId }, { domainName: domainName }] }, function (err, organization) {
            if (err) {
                reject(err)
            }
            else {
                if (organization == null) {
                    resolve()
                }
                else {
                    reject({ code: 409 })
                }
            }
        })
    });
}


function login(query) {
    return new Promise(function (resolve, reject) {
        Organization.findOne(
            query
            , function (err, organization) {
                if (!err) {
                    if (organization != null) {
                        resolve(organization);
                    } else {
                        reject({ code: 401 });
                    }
                } else {
                    reject(err);
                }
            });
    });
};

function findOrganizationById(organizationId) {
    return new Promise(function (resolve, reject) {
        Organization.findById(organizationId, function (err, organization) {
            if (err) {
                reject(err)
            }
            else {
                if (organization == null) {
                    reject({ code: 409 })
                }
                else {
                    resolve(organization)
                }
            }
        })
    });
}

function getAllBanks()
{
    return new Promise((resolve,reject)=>{
        Organization.find({role: "Bank"})
        .then((data)=>resolve(data))
        .catch(err =>reject(err));
    })
}

function getBankByName(query){
    return Organization.findOne(query);
}

module.exports = organizationDao;
