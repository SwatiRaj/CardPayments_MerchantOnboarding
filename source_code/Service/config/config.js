var fs = require('fs');
var logger = require('../config/logger');
var environment = 'LOCAL';

var properties = {
    LOCAL: {
        app: {
            host: "localhost",
            port: 51030
        },
        db: {
            username: "****",
            password: "********",
            host: "IP_ADDRESS",
            port: 51010,
            dbName: 'MerchantOnboarding'
        },
        logs: {
            location: 'logs'
        }
    },
    //Running services on VM and connecting to DB on same VM
    PROD: {
        app: {
            host: "localhost",
            port: 51030
        },
        db: {
            host: 'IP_ADDRESS',
            port: 51010,
            username: "****",
            password: "********",
            dbName: 'MerchantOnboarding'
        },
        logs: {
            location: 'logs'
        }
    }
};

var getProperties = function () {
    return properties[environment];
}

var setEnvironment = function (newEnvironment) {
    if (properties[newEnvironment]) {
        environment = newEnvironment;
    }
    logger.info("Setting up properties for ", environment, " environment");
};

var getEnvironment = function () {
    return environment
};

module.exports = {
    properties: getProperties,
    setEnvironment: setEnvironment,
    getEnvironment: getEnvironment
};
