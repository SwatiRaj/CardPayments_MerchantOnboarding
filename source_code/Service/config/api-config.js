var express = require("express");
var properties = require('./config').properties();
var path = require('path');
var bodyParser = require("body-parser");
var merchantController = require("../app/controllers/merchant.controller");
var networkController = require("../app/controllers/network.controller");
var bankController = require('../app/controllers/bank.controller');
var appController = require('../app/controllers/app.controller');
var nocAuthorityController = require('../app/controllers/nocAuthority.controller')

var morganLogger = require("morgan");
var Response = require("../app/util/response");
var logger = require("../config/logger");
var fs = require('fs');
var routes = require('../app/routes/routes');
var authentication = require('./authentication');
var cors = require("cors");


// Mock service for Tan
var tanController = require("../TanService/controller/tan.controller")
var mockRoutes = require('../TanService/routes/routes');


var app = express();
app.use(authentication.initialize());
app.use(morganLogger('dev'));
var router = express.Router();
app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ limit: '10mb', "extended": false }));
app.use('/', cors())
app.use('/', router);

app.use("/merchant-onboarding-services/apis", express.static(__dirname + '/../public/'));

var controllers = {
    appController: appController,
    merchantController: merchantController,
    networkController: networkController,
    bankController: bankController,
    nocAuthorityController: nocAuthorityController,

};

routes.setUp(router, controllers);

//Mock service 
var mockControllers = {
    tanController: tanController
};

mockRoutes.setUp(router, mockControllers);

app.use(function (req, res) {
    res.status(404).json({ url: req.originalUrl + ' not found' })
});

//error handler if something breaks
app.use(function (err, req, res, next) {
    logger.error(err.stack);
    res.status(500).send('Something broke!');
});


if (!fs.existsSync(properties.logs.location)) {
    fs.mkdirSync(properties.logs.location);
}

var ApiConfig = function () {
    this.app = app;
}

module.exports = ApiConfig;
