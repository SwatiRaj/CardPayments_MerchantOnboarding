var passport = require("passport");
var passportJWT = require("passport-jwt");
var jwtConfig = require("./jwtConfig");
var ExtractJwt = passportJWT.ExtractJwt;
var Strategy = passportJWT.Strategy;
var params = {
    secretOrKey: jwtConfig.jwtSecret,
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
};
var log4js = require('log4js');
var logger = log4js.getLogger('Authenticate User');
logger.setLevel('DEBUG');

var merchantDao = require('../app/dao/merchant.dao');
var organizationDao = require('../app/dao/organization.dao');

function initialize() {
    var newStrategy = new Strategy(params, function (payload, done) {
        if (payload.role == "Merchant") {
            merchantDao.findMerchantById(payload.id).then((merchant) => {
                return done(null, merchant);
            }).catch((err) => {
                return done(null, false);
            });
        }
        else {
            organizationDao.findOrganizationById(payload.id).then((org) => {
                return done(null, org)
            }).catch((err) => {
                return done(null, false)
            });
        }

    });
    passport.use(newStrategy);
    return passport.initialize();
}

function authenticate() {
    return passport.authenticate("jwt", jwtConfig.jwtSession);
}

module.exports = {
    initialize: initialize,
    authenticate: authenticate
}

