var util = require('util');
var path = require('path');
var hfc = require('fabric-client');
var fs = require('fs');

function loadOrgsProfile() {

	hfc.setConfigSetting('network-connection-profile-path', path.join(__dirname, '..', 'artifacts', 'networkConfig', "networkConfig.json"));
	hfc.addConfigFile(path.join(__dirname, 'hfcConfig.json'));

	orgList = JSON.parse(fs.readFileSync(__dirname + "/../artifacts/networkConfig/orgProfileTrack.json"));
	for (var i = 0; i < orgList.orgs.length; i++) {
		hfc.setConfigSetting(orgList.orgs[i] + '-connection-profile-path', path.join(__dirname, '..', 'artifacts', 'organizations', orgList.orgs[i], orgList.orgs[i] + '.profile.json'));
	}
}
loadOrgsProfile();
module.exports.loadOrgsProfile = loadOrgsProfile
