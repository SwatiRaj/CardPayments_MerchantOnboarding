var config = require("./config");
var properties = config.properties();
var mongoose = require("mongoose");
mongoose.Promise = require('bluebird');
// var logger = require("../config/logger");
var log4js = require('log4js');
var logger = log4js.getLogger('Mongoose connection');

function connect() {
    var url = 'mongodb://' + properties.db.host.trim() + ':' + properties.db.port + '/' + properties.db.dbName;
    var opt = {
        auth: {
            authdb: 'admin'
        }
    };

    logger.info("Mongo db url : " + url);
    mongoose.connect(url, { useMongoClient: true }).then(
        () => {
            logger.info("Connection to Mongo established successfully..");
        },
        (err) => {
            logger.error('Connection to mongo failed ' + err);
        }
    );
}

var Mongoose = function () {
    this.connect = connect;
}

module.exports = Mongoose;
