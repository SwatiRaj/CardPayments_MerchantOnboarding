var configObj = require("./config/config");
configObj.setEnvironment(process.argv[2]);
var properties = configObj.properties();
// var logger = require("./config/logger");
var log4js = require('log4js');
var logger = log4js.getLogger('Main App');
var Mongoose = require("./config/mongoose");
var mongo = new Mongoose();
var Apis = require("./config/api-config");
require('./config/hfcConfig');
var apis = new Apis();
var networkConfig = require('./artifacts/networkConfig/networkConfig.json')
var host = networkConfig.ipAddress;
var port = networkConfig.appPort;

var server = apis.app.listen(port, function () {
});

mongo.connect();

logger.info('****************** SERVER STARTED ************************');
logger.info('***************  http://%s:%s  ******************', host, port);
